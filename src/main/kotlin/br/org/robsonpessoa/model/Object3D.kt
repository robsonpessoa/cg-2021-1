package br.org.robsonpessoa.model

import br.org.robsonpessoa.glfw.*

interface Object3DSignature : Drawable, WithCoordinates<Triple<Float, Float, Float>>, Texturizable, Normalizable {
    val shape: Triple<Float, Float, Float>
    val faces: List<Object3DFace>
}

open class Object3D(override val faces: List<Object3DFace>) : Object3DSignature {

    override val shape: Triple<Float, Float, Float>
        get() = lazyShape.value

    private val lazyShape: Lazy<Triple<Float, Float, Float>> = lazy {
        val max = faces.fold(Triple(Float.MIN_VALUE, Float.MIN_VALUE, Float.MIN_VALUE)) { acc, face ->
            acc.copy(
                first = maxOf(acc.first, face.coordinates.maxByOrNull { it.first }?.first ?: Float.MIN_VALUE),
                second = maxOf(acc.second, face.coordinates.maxByOrNull { it.second }?.second ?: Float.MIN_VALUE),
                third = maxOf(acc.third, face.coordinates.maxByOrNull { it.third }?.third ?: Float.MIN_VALUE)
            )
        }
        val min = faces.fold(Triple(Float.MAX_VALUE, Float.MAX_VALUE, Float.MAX_VALUE)) { acc, face ->
            acc.copy(
                first = minOf(acc.first, face.coordinates.minByOrNull { it.first }?.first ?: Float.MAX_VALUE),
                second = minOf(acc.second, face.coordinates.minByOrNull { it.second }?.second ?: Float.MAX_VALUE),
                third = minOf(acc.third, face.coordinates.minByOrNull { it.third }?.third ?: Float.MAX_VALUE)
            )
        }
        return@lazy Triple(
            max.first - min.first,
            max.second - min.second,
            max.third - min.third
        )
    }

    override fun draw(engine: Engine) {
        faces.forEach { it.draw(engine) }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        faces.groupBy { it.material }.forEach { (_, texturedFaces) ->
            texturedFaces.forEach { it.configureVertices(builder) }
        }
    }

    override fun configureTextures(builder: ApplicationBuilder) {
        faces.groupBy { it.material }.forEach { (_, texturedFaces) ->
            texturedFaces.forEach { it.configureTextures(builder) }
        }
    }

    override fun configureNormals(builder: ApplicationBuilder) {
        faces.groupBy { it.material }.forEach { (_, texturedFaces) ->
            texturedFaces.forEach { it.configureNormals(builder) }
        }
    }

    override fun getCenter(): Triple<Float, Float, Float> {
        val centers = faces.map { it.getCenter() }
        val centerSum = centers.fold(Triple(0f, 0f, 0f)) { acc, center ->
            acc.copy(
                first = acc.first + center.first,
                second = acc.second + center.second,
                third = acc.third + center.third
            )
        }
        return centerSum.copy(
            first = centerSum.first / faces.size,
            second = centerSum.second / faces.size,
            third = centerSum.third / faces.size
        )
    }
}
