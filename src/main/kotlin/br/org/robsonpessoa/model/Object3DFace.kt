package br.org.robsonpessoa.model

import br.org.robsonpessoa.glfw.*
import br.org.robsonpessoa.matrix.IlluminationController
import br.org.robsonpessoa.primitives.Primitive
import org.lwjgl.opengl.GL11
import java.util.*

class Object3DFace(
    private val vertices: FloatArray,
    val material: String? = null,
    private val textures: FloatArray = floatArrayOf(),
    private val normals: FloatArray = floatArrayOf(),
    private val glDrawMethod: Int,
    override var color: Engine.Color? = null,
    var ambientCoefficient: Float,
    private val diffuseCoefficient: Float,
    private val specularCoefficient: Float,
    private val specularExponent: Float
) : Primitive, WithCoordinates<Triple<Float, Float, Float>>, Drawable, Texturizable, Normalizable {

    override val id: UUID = UUID.randomUUID()
    override val name: String = "face-object-$id"

    var texture: Texture? = null

    override fun draw(engine: Engine) {
        val application = engine.getApplication()!!
        application[name]?.let {
            if (texture != null) {
                texture!!.use {
                    draw(application.id, it)
                }
            } else {
                color?.let { color ->
                    setColor(engine, color)
                } ?: setColor(engine, engine.foregroundColor)

                draw(application.id, it)

                setColor(engine, engine.foregroundColor)
            }
        }
    }

    private fun draw(id: Int, it: Application.ObjectVertexData) {
        IlluminationController
            .withIllumination(
                id,
                ambientCoefficient,
                diffuseCoefficient,
                specularCoefficient,
                specularExponent
            ) {
                GL11.glDrawArrays(
                    glDrawMethod,
                    it.start,
                    it.length
                )
            }
    }

    override fun getCenter(): Triple<Float, Float, Float> {
        var pos = 0
        val centerSum = vertices.fold(Triple(0f, 0f, 0f)) { acc, coordinate ->
            val result = when (pos) {
                0 -> acc.copy(first = acc.first + coordinate)
                1 -> acc.copy(second = acc.second + coordinate)
                2 -> acc.copy(third = acc.third + coordinate)
                else -> acc
            }
            pos = (pos + 1) % 3
            result
        }
        val n = vertices.size / 3
        return centerSum.copy(
            first = centerSum.first / n,
            second = centerSum.second / n,
            third = centerSum.third / n
        )
    }

    val coordinates: List<Triple<Float, Float, Float>>
        get() {
            val coordinateList = mutableListOf<Triple<Float, Float, Float>>()
            for (i in vertices.indices step 3) {
                coordinateList.add(Triple(vertices[i], vertices[i + 1], vertices[i + 2]))
            }
            return coordinateList
        }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            vertices
        }
    }

    override fun configureTextures(builder: ApplicationBuilder) {
        builder.addTexture(name) {
            if (texture != null) textures else FloatArray(textures.size) { 0f }
        }
    }

    override fun configureNormals(builder: ApplicationBuilder) {
        builder.addNormals(name) {
            normals
        }
    }


}
