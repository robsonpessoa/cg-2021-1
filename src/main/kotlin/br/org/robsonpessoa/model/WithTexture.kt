package br.org.robsonpessoa.model

import br.org.robsonpessoa.glfw.*

interface Normalized {
    val obj: Normalizable

    fun configureNormals(builder: ApplicationBuilder) {
        obj.configureNormals(builder)
    }
}

interface Textured {
    val obj: Texturizable

    fun configureTextures(builder: ApplicationBuilder) {
        obj.configureTextures(builder)
    }
}

object TextureData {
    val map: MutableMap<String, Texture> = mutableMapOf()
}

data class WithTexture(
    private val materials: Map<String, String>,
    override val obj: Object3DSignature,
    val flipVertically: Boolean = true,
    val flipHorizontally: Boolean = false,
    private val fallback: String? = null
) :
    Object3DSignature by obj,
    WithCoordinates<Triple<Float, Float, Float>>, Textured, Normalizable {

    private fun createTextures() {
        materials.forEach { (materialName, filename) ->
            if (!TextureData.map.containsKey(materialName)) {
                val texture = createTexture(filename)
                TextureData.map[materialName] = texture
            }

            val texture: Texture = TextureData.map[materialName]!!

            obj.faces.filter { it.material != null && materialName == it.material && it.texture == null }
                .forEach {
                    it.texture = texture
                }
        }

        if (fallback != null && obj.faces.any { it.material != null && it.texture == null }) {
            val texture = createTexture(fallback)

            obj.faces.filter { it.material != null && it.texture == null }.forEach {
                TextureData.map[it.material!!] = texture
                it.texture = texture
            }
        }
    }

    private fun createTexture(filename: String): Texture {
        val textureId = TextureFactory(filename).create(flipVertically, flipHorizontally)
            ?: throw RuntimeException("The texture file $filename could not be loaded")
        return Texture(textureId)
    }

    companion object {
        const val VERTEX_NAME = "texture_coord"
    }

    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureTextures(builder: ApplicationBuilder) {
        createTextures()
        obj.configureTextures(builder)
    }

    override fun getCenter(): Triple<Float, Float, Float> {
        return obj.getCenter()
    }
}
