package br.org.robsonpessoa.model

import org.lwjgl.opengl.GL11

class Texture(val id: Int) {

    fun use(f: () -> Unit) {
        GL11.glEnable(GL11.GL_TEXTURE_2D)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, id)

        f()

        GL11.glDisable(GL11.GL_TEXTURE_2D)
    }
}
