package br.org.robsonpessoa.model

interface WithCoordinates<T> {
    fun getCenter(): T
}
