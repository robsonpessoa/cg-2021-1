package br.org.robsonpessoa.math

import br.org.robsonpessoa.utils.toBuffer
import org.joml.Matrix4f
import org.joml.Vector4f
import java.nio.FloatBuffer

/**
 * A class that represents a Matrix of Floats filled with zero.
 *
 * @property rows the number of rows.
 * @property columns the number of columns.
 */
@Deprecated("Please use the methods from org.joml.* instead")
open class Matrix(private val rows: Int, private val columns: Int) {
    protected var data = Array(rows) { FloatArray(columns) }

    companion object {
        fun Matrix4f.toMatrix(): Matrix {
            val matrix = Matrix(4, 4)

            matrix.data[0][0] = m00()
            matrix.data[0][1] = m01()
            matrix.data[0][2] = m02()
            matrix.data[0][3] = m03()

            matrix.data[1][0] = m10()
            matrix.data[1][1] = m11()
            matrix.data[1][2] = m12()
            matrix.data[1][3] = m13()

            matrix.data[2][0] = m20()
            matrix.data[2][1] = m21()
            matrix.data[2][2] = m22()
            matrix.data[2][3] = m23()

            matrix.data[3][0] = m30()
            matrix.data[3][1] = m31()
            matrix.data[3][2] = m32()
            matrix.data[3][3] = m33()

            return matrix
        }
    }

    /**
     * Gets the value of the param (x, y).
     *
     * @param x the x coordinate.
     * @param y the y coordinate.
     * @return the value.
     */
    operator fun get(x: Int, y: Int): Float = data[x][y]

    /**
     * Sets the value (x,y) of the Matrix.
     *
     * @param x the x coordinate.
     * @param y the y coordinate.
     * @param value the value to be set.
     */
    operator fun set(x: Int, y: Int, value: Float) {
        data[x][y] = value
    }

    init {
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                this.data[i][j] = 0.0f
            }
        }
    }

    /**
     * The product of two [Matrix]'es.
     *
     * @param other the other [Matrix].
     * @return the product [Matrix].
     */
    operator fun times(other: Matrix): Matrix {
        val product = Matrix(rows, other.columns)

        for (i in 0 until rows) {
            for (j in 0 until other.columns) {
                for (k in 0 until columns) {
                    product.data[i][j] += data[i][k] * other.data[k][j]
                }
            }
        }

        return product
    }

    /**
     * Creates an array of Float based on the Matrix values.
     *
     * @return the array.
     */
    private fun toArray(): FloatArray {
        return FloatArray(rows * columns).apply {
            var index = 0
            (0 until rows).forEach { row ->
                (0 until columns).forEach { column ->
                    this[index++] = data[row][column]
                }
            }
        }
    }

    /**
     * Creates a buffer of Float based on the Matrix values.
     *
     * @return the buffer.
     */
    fun toBuffer(): FloatBuffer {
        return toMatrix4f().toBuffer()
    }

    fun toMatrix4f(): Matrix4f {
        val matrix = Matrix4f()
        matrix.setRow(0, Vector4f(data[0][0], data[0][1], data[0][2], data[0][3]))
        matrix.setRow(1, Vector4f(data[1][0], data[1][1], data[1][2], data[1][3]))
        matrix.setRow(2, Vector4f(data[2][0], data[2][1], data[2][2], data[2][3]))
        matrix.setRow(3, Vector4f(data[3][0], data[3][1], data[3][2], data[3][3]))
        return matrix
    }

    /**
     * Get's a [String] representing the Matrix values.
     *
     * @return the [String] object.
     */
    override fun toString(): String {
        var str = ""
        (0 until rows).forEach { row ->
            (0 until columns).forEach { column ->
                if (column == 0) {
                    str += "|"
                }

                str += "\t\t${data[row][column]}\t\t"

                if (column == columns - 1) {
                    str += "|\n"
                }
            }
        }
        return str
    }
}

/**
 * A Matrix with the main diagonal filled with 1.
 *
 * @property dimension the dimension of the Matrix.
 */
data class IdentityMatrix(val dimension: Int) : Matrix(dimension, dimension) {
    init {
        for (i in 0 until dimension) {
            this.data[i][i] = 1.0f
        }
    }

    override fun toString(): String {
        return super.toString()
    }
}

/**
 * Creates a buffer of Float based on the Matrix values.
 *
 * @return the buffer.
 */
fun Matrix4f.toBuffer(): FloatBuffer {
    val ref = FloatArray(16)
    get(ref)
    return ref.toBuffer()
}
