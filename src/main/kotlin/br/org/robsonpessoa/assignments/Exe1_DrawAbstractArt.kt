//package br.org.robsonpessoa.assignments
//
//import br.org.robsonpessoa.glfw.ApplicationBuilder
//import br.org.robsonpessoa.glfw.Engine
//import br.org.robsonpessoa.glsl.FragmentShader
//import br.org.robsonpessoa.glsl.VertexShader
//import br.org.robsonpessoa.primitives.*
//
//class DrawAbstractArt(private val engine: Engine) : Engine.EngineListener {
//
//    private val primitives = mutableListOf<Primitive>()
//
//    init {
//        val line: Primitive = Line(
//            floatArrayOf(
//                -0.5f, 0.5f,
//                -0.5f, -0.5f,
//                +0.5f, -0.5f,
//                +0.5f, +0.5f
//            )
//        )
//
//        val circle: Primitive = Circle(
//            0.2f,
//            -0.2f, 0.2f
//        )
//        circle.color = Engine.Color(1f, 0f, 0f)
//
//        val triangle: Primitive = Triangle(
//            first = Pair(0f, 0f),
//            second = Pair(0.5f, 0.5f),
//            third = Pair(0.5f, 0f)
//        )
//        triangle.color = Engine.Color(0f, 0.8f, 0f)
//
//        val square: Primitive = Square(
//            floatArrayOf(
//                0.1f, -0.1f,
//                0.1f, 0.1f,
//                -0.1f, -0.1f,
//                -0.1f, 0.1f
//            )
//        )
//        square.color = Engine.Color(0f, 0f, 0.8f)
//
//        val point = Point(0f, 0f)
//        point.color = Engine.Color(1f, 1f, 1f)
//
//        primitives.add(line)
//        primitives.add(circle)
//        primitives.add(triangle)
//        primitives.add(square)
//        primitives.add(point)
//    }
//
//    override fun onBuildProgram(builder: ApplicationBuilder) {
//        configureShaders(builder)
//        configureVertices(builder)
//    }
//
//    override fun onDraw() {
//        primitives.forEach {
//            it.setColor(engine, engine.foregroundColor)
//            it.draw(engine)
//        }
//    }
//
//    private fun configureVertices(builder: ApplicationBuilder) {
//        primitives.forEach {
//            it.configureVertices(builder)
//        }
//    }
//
//    private fun configureShaders(builder: ApplicationBuilder) {
//        builder.addShader(
//            VertexShader()
//        )
//
//        builder.addShader(
//            FragmentShader()
//        )
//    }
//}
