package br.org.robsonpessoa.assignments

import br.org.robsonpessoa.glfw.Application
import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnKeyListener
import br.org.robsonpessoa.glsl.FragmentShader
import br.org.robsonpessoa.glsl.VertexShader
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.primitives.Square
import br.org.robsonpessoa.primitives.Triangle
import br.org.robsonpessoa.transformation.MutablePrimitive
import br.org.robsonpessoa.transformation.Rotation
import br.org.robsonpessoa.transformation.Scale
import br.org.robsonpessoa.transformation.Translation

class Question1(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val triangle = MutablePrimitive(
        Triangle(
            first = Pair(0f, 0f),
            second = Pair(0.5f, 0.5f),
            third = Pair(0.5f, 0f)
        )
    )

    private val primitives = listOf(triangle)

    init {
        triangle.color = Engine.Color(0f, 0.8f, 0f)
    }

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {
        TODO("Not yet implemented")
    }

    override fun onDraw() {
        primitives.forEach {
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        scale(scanCode, action)
    }

    private fun scale(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.ARROW_RIGHT.code && OnKeyListener.Action.downOrPressed(action)) {
            triangle.apply(Scale(0.95f, 0.95f, 0f))
        } else if (scanCode == OnKeyListener.Button.ARROW_LEFT.code && OnKeyListener.Action.downOrPressed(action)) {
            triangle.apply(Scale(1.05f, 1.05f, 0f))
        }
    }
}

class Question2(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val square = MutablePrimitive(
        Square(
            floatArrayOf(
                0.1f, -0.1f,
                0.1f, 0.1f,
                -0.1f, -0.1f,
                -0.1f, 0.1f
            )
        )
    )

    private val primitives = listOf(square)

    init {
        square.color = Engine.Color(0f, 0f, 0.8f)
    }

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {
        TODO("Not yet implemented")
    }

    override fun onDraw() {
        primitives.forEach {
            it.setColor(engine, engine.foregroundColor)
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        if (scanCode == OnKeyListener.Button.ARROW_RIGHT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(-0.5))
        } else if (scanCode == OnKeyListener.Button.ARROW_LEFT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(0.5))
        }
    }
}

class Question3(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val circle = MutablePrimitive(
        Circle(
            0.2f,
            -0.2f, 0.2f
        )
    )

    private val primitives = listOf(circle)

    init {
        circle.color = Engine.Color(1f, 0f, 0f)
    }

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {
        TODO("Not yet implemented")
    }

    override fun onDraw() {
        primitives.forEach {
            it.setColor(engine, engine.foregroundColor)
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        translate(scanCode, action)
    }

    private fun translate(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.W.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0f, 0.05f, 0f))
        } else if (scanCode == OnKeyListener.Button.S.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0f, -0.05f, 0f))
        } else if (scanCode == OnKeyListener.Button.A.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(-0.05f, 0f, 0f))
        } else if (scanCode == OnKeyListener.Button.D.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0.05f, 0f, 0f))
        }
    }
}

class Question4(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val circle = MutablePrimitive(
        Circle(
            0.2f,
            -0.2f, 0.2f
        )
    )

    private val square = MutablePrimitive(
        Square(
            floatArrayOf(
                0.1f, -0.1f,
                0.1f, 0.1f,
                -0.1f, -0.1f,
                -0.1f, 0.1f
            )
        )
    )

    private val primitives = listOf(circle, square)

    init {
        circle.color = Engine.Color(1f, 0f, 0f)
        square.color = Engine.Color(0f, 0f, 0.8f)
    }

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {
        TODO("Not yet implemented")
    }

    override fun onDraw() {
        primitives.forEach {
            it.setColor(engine, engine.foregroundColor)
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        rotate(scanCode, action)
        translate(scanCode, action)
    }

    private fun translate(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.W.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0f, 0.05f, 0f))
            square.apply(Translation(0f, 0.025f, 0f))
        } else if (scanCode == OnKeyListener.Button.S.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0f, -0.05f, 0f))
            square.apply(Translation(0f, -0.025f, 0f))
        } else if (scanCode == OnKeyListener.Button.A.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(-0.05f, 0f, 0f))
            square.apply(Translation(-0.025f, 0f, 0f))
        } else if (scanCode == OnKeyListener.Button.D.code && OnKeyListener.Action.downOrPressed(action)) {
            circle.apply(Translation(0.05f, 0f, 0f))
            square.apply(Translation(0.025f, 0f, 0f))
        }
    }

    private fun rotate(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.ARROW_RIGHT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(-0.5))
        } else if (scanCode == OnKeyListener.Button.ARROW_LEFT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(0.5))
        }
    }
}

class Question5(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val square = MutablePrimitive(
        Square(
            floatArrayOf(
                0.1f, -0.1f,
                0.1f, 0.1f,
                -0.1f, -0.1f,
                -0.1f, 0.1f
            )
        )
    )

    private val primitives = listOf(square)

    init {
        square.color = Engine.Color(0f, 0f, 0.8f)
    }

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {
        TODO("Not yet implemented")
    }

    override fun onDraw() {
        primitives.forEach {
            it.setColor(engine, engine.foregroundColor)
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        rotate(scanCode, action)
        translate(scanCode, action)
        scale(scanCode, action)
    }

    private fun translate(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.W.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Translation(0f, 0.025f, 0f))
        } else if (scanCode == OnKeyListener.Button.S.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Translation(0f, -0.025f, 0f))
        } else if (scanCode == OnKeyListener.Button.A.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Translation(-0.025f, 0f, 0f))
        } else if (scanCode == OnKeyListener.Button.D.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Translation(0.025f, 0f, 0f))
        }
    }

    private fun rotate(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.ARROW_RIGHT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(-0.5))
        } else if (scanCode == OnKeyListener.Button.ARROW_LEFT.code && OnKeyListener.Action.downOrPressed(action)) {
            square.apply(Rotation(0.5))
        }
    }

    private fun scale(scanCode: Int, action: Int) {
        if (scanCode == OnKeyListener.Button.ARROW_DOWN.code && OnKeyListener.Action.downOrPressed(action)) {
            val (x, y) = square.getPrimitive().getCenter()
            square.apply(Translation(-x, -y, 0f))
            square.apply(Scale(0.95f, 0.95f, 0f))
            square.apply(Translation(x, y, 0f))
        } else if (scanCode == OnKeyListener.Button.ARROW_UP.code && OnKeyListener.Action.downOrPressed(action)) {
            val (x, y) = square.getPrimitive().getCenter()
            square.apply(Translation(-x, -y, 0f))
            square.apply(Scale(1.05f, 1.05f, 0f))
            square.apply(Translation(x, y, 0f))
        }
    }
}
