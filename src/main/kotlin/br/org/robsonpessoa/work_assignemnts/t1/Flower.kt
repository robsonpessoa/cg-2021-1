package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnTimeTickListener
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.primitives.Line
import br.org.robsonpessoa.primitives.Triangle
import br.org.robsonpessoa.transformation.MutablePrimitive
import br.org.robsonpessoa.transformation.Rotation
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Class that represents a drawable flower object.
 *
 * This object has an individual transformation that
 * is triggered each 400 milliseconds. The transformation
 * is responsible to rotate the flower bole left to represent
 * an animation of the flower moving with the wind.
 *
 * It's composed by some primitives:
 * - [Line] for the bole,
 * - [Circle] for the flower base,
 * - [Triangle] for the two flower petals at the top;
 *
 */
class Flower(private val x: Float, private val y: Float, private val color: Engine.Color) : Drawable,
    OnTimeTickListener {
    private var direction = 1
    private val angle = Math.PI / 32

    companion object {
        private val brownColor = Engine.Color(144, 93, 39)
        private const val LINE_SIZE = 0.05f
        private const val FLOWER_RADIUS = 0.02f
    }

    private val line = MutablePrimitive(
        Line(
            x, y,
            x, y + LINE_SIZE
        )
    )

    private val base = MutablePrimitive(
        Circle(
            FLOWER_RADIUS, x, y + LINE_SIZE + FLOWER_RADIUS * 0.7f
        )
    )

    private val petal1 = MutablePrimitive(
        Triangle(
            Pair(x - FLOWER_RADIUS, y + LINE_SIZE + FLOWER_RADIUS),
            Pair(x - FLOWER_RADIUS, y + LINE_SIZE + 2 * FLOWER_RADIUS),
            Pair(x - FLOWER_RADIUS / 2, y + LINE_SIZE + FLOWER_RADIUS)
        )
    )

    private val petal2 = MutablePrimitive(
        Triangle(
            Pair(x + FLOWER_RADIUS, y + LINE_SIZE + FLOWER_RADIUS),
            Pair(x + FLOWER_RADIUS, y + LINE_SIZE + 2 * FLOWER_RADIUS),
            Pair(x + FLOWER_RADIUS / 2, y + LINE_SIZE + FLOWER_RADIUS)
        )
    )

    private val primitives = listOf(line, petal1, petal2, base)

    init {
        line.color = brownColor
        base.color = color
        petal1.color = color
        petal2.color = color

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate({
            this.onTimeTick()
        }, 500, 400, TimeUnit.MILLISECONDS)
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    override fun onTimeTick() {
        primitives.forEach {
            it.apply(Rotation(x, y, angle * direction))
        }
        direction *= -1;
    }

}
