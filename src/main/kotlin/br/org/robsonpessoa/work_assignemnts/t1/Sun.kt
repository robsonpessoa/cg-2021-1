package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnTimeTickListener
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.primitives.Line
import br.org.robsonpessoa.transformation.MutablePrimitive
import br.org.robsonpessoa.transformation.Rotation
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Class that represents a drawable sun object.
 *
 * This object has an individual transformation that
 * is triggered each 500 milliseconds. The transformation
 * is responsible to rotate the sun lights around the sun.
 *
 * It's composed by some primitives:
 * - [Line] for the lights,
 * - [Circle] for the sun;
 *
 */
class Sun : OnTimeTickListener, Drawable {
    private var rotation = 0.0

    companion object {
        private val yellowColor = Engine.Color(0.96f, 0.78f, 0.22f)
        private const val CENTER_X = 0.6f
        private const val CENTER_Y = 0.7f
        private const val RADIUS = 0.1f
        private const val LIGHT_SHIFT = 0.05f
        private const val LIGHT_SIZE = 0.1f
    }

    private val circle = Circle(
        RADIUS,
        CENTER_X, CENTER_Y
    )

    private val light1 = MutablePrimitive(
        Line(
            CENTER_X, CENTER_Y + RADIUS + LIGHT_SHIFT,
            CENTER_X, CENTER_Y + RADIUS + LIGHT_SHIFT + LIGHT_SIZE
        )
    )

    private val light2 = MutablePrimitive(
        Line(
            CENTER_X - RADIUS - LIGHT_SHIFT, CENTER_Y,
            CENTER_X - RADIUS - LIGHT_SHIFT - LIGHT_SIZE, CENTER_Y
        )
    )

    private val light3 = MutablePrimitive(
        Line(
            CENTER_X + RADIUS + LIGHT_SHIFT, CENTER_Y,
            CENTER_X + RADIUS + LIGHT_SHIFT + LIGHT_SIZE, CENTER_Y
        )
    )

    private val light4 = MutablePrimitive(
        Line(
            CENTER_X, CENTER_Y - RADIUS - LIGHT_SHIFT,
            CENTER_X, CENTER_Y - RADIUS - LIGHT_SHIFT - LIGHT_SIZE
        )
    )

    private val primitives = listOf(circle, light1, light2, light3, light4)

    init {
        circle.color = yellowColor
        light1.color = yellowColor
        light2.color = yellowColor
        light3.color = yellowColor
        light4.color = yellowColor

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate({
            this.onTimeTick()
        }, 1, 500, TimeUnit.MILLISECONDS)
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    override fun onTimeTick() {
        this.rotation += Math.PI / 4
        if (this.rotation == Math.PI) {
            this.rotation = 0.0
        }
        listOf(light1, light2, light3, light4).forEach {
            it.apply(Rotation(CENTER_X, CENTER_Y, this.rotation))
        }
    }

}
