package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnKeyListener
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.primitives.Square
import br.org.robsonpessoa.transformation.MutablePrimitive
import br.org.robsonpessoa.transformation.Rotation
import br.org.robsonpessoa.transformation.Scale
import br.org.robsonpessoa.transformation.Translation
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.math.abs

/**
 * Class that represents a drawable person object.
 * It is the most complex object in this assignment context.
 *
 * By using the keyboard arrows, it's possible to move the person
 * right, left, front and back in the field.
 * When the object moves, its arms and legs also moves up and down
 * and its eyes changes according with the object change of direction.
 *
 * It's composed by some primitives:
 * - [Square] for the body, arms and legs,
 * - [Circle] for the head.
 *
 */
class Person(private val x: Float, private val y: Float) : Drawable, OnKeyListener {
    private var lookingRight = true
    private var lookingFront = false
    private var lookingBack = false

    private var rightLegFirst = true
    private var state = State.STOPPED

    private enum class State {
        STOPPED,
        WALKING,
        MOVED
    }

    companion object {
        private val brownColor = Engine.Color(156, 113, 67)
        private val grayColor = Engine.Color(158, 156, 154)
        private val grayColorBack = Engine.Color(138, 136, 134)
        private val darkerGrayColor = Engine.Color(51, 51, 50)
        private val darkerGrayColorBack = Engine.Color(40, 40, 40)

        private const val HEAD_RADIUS = 0.04f
        private const val BODY_HEIGHT = 0.1f
        private const val BODY_WIDTH = 0.04f
        private const val LEG_HEIGHT = 0.04f
        private const val LEG_WIDTH = 0.02f
        private const val ARM_HEIGHT = 0.03f
        private const val ARM_WIDTH = 0.02f
        private const val STEP_DISTANCE = 0.04f
    }

    private val head = MutablePrimitive(
        Circle(
            HEAD_RADIUS, x, y + LEG_HEIGHT + BODY_HEIGHT + HEAD_RADIUS / 2
        )
    )

    private val eye1 = MutablePrimitive(
        Circle(
            0.005f, x, y + LEG_HEIGHT + BODY_HEIGHT + HEAD_RADIUS / 2
        )
    )

    private val eye2 = MutablePrimitive(
        Circle(
            0.005f, x, y + LEG_HEIGHT + BODY_HEIGHT + HEAD_RADIUS / 2
        )
    )

    private val body = MutablePrimitive(
        Square(
            floatArrayOf(
                x - BODY_WIDTH / 2, y + LEG_HEIGHT,
                x + BODY_WIDTH / 2, y + LEG_HEIGHT,
                x - BODY_WIDTH / 2, y + LEG_HEIGHT + BODY_HEIGHT,
                x + BODY_WIDTH / 2, y + LEG_HEIGHT + BODY_HEIGHT
            )
        )
    )

    private val leftArm = MutablePrimitive(
        Square(
            floatArrayOf(
                x - ARM_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT + BODY_HEIGHT - ARM_HEIGHT,
                x + ARM_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT + BODY_HEIGHT - ARM_HEIGHT,
                x - ARM_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT + ARM_HEIGHT,
                x + ARM_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT + ARM_HEIGHT
            )
        )
    )

    private val rightArm = MutablePrimitive(
        Square(
            floatArrayOf(
                x - ARM_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT + BODY_HEIGHT - ARM_HEIGHT,
                x + ARM_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT + BODY_HEIGHT - ARM_HEIGHT,
                x - ARM_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT + ARM_HEIGHT,
                x + ARM_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT + ARM_HEIGHT
            )
        )
    )

    private val leftLeg = MutablePrimitive(
        Square(
            floatArrayOf(
                x - LEG_WIDTH / 2 - BODY_WIDTH / 4, y,
                x + LEG_WIDTH / 2 - BODY_WIDTH / 4, y,
                x - LEG_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT,
                x + LEG_WIDTH / 2 - BODY_WIDTH / 4, y + LEG_HEIGHT
            )
        )
    )

    private val rightLeg = MutablePrimitive(
        Square(
            floatArrayOf(
                x - LEG_WIDTH / 2 + BODY_WIDTH / 4, y,
                x + LEG_WIDTH / 2 + BODY_WIDTH / 4, y,
                x - LEG_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT,
                x + LEG_WIDTH / 2 + BODY_WIDTH / 4, y + LEG_HEIGHT
            )
        )
    )

    private val primitives = listOf(leftLeg, leftArm, body, head, eye1, eye2, rightArm, rightLeg)

    init {
        head.color = brownColor
        eye1.color = darkerGrayColorBack
        eye2.color = Engine.Color(darkerGrayColorBack, 0f)
        body.color = grayColor
        leftArm.color = grayColorBack
        rightArm.color = grayColor
        leftLeg.color = darkerGrayColorBack
        rightLeg.color = darkerGrayColor

        eye1.apply(Translation(HEAD_RADIUS / 2, 0f))
        eye2.apply(Translation(-HEAD_RADIUS / 2, 0f))
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
        if (this.state == State.MOVED) {
            this.state = State.STOPPED
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        if ((lookingFront || lookingBack)
            && (scanCode == OnKeyListener.Button.ARROW_RIGHT.code
                    || scanCode == OnKeyListener.Button.ARROW_LEFT.code)
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            stopLookingVertical()
        }

        configureEyes(scanCode, action)
        move(scanCode, action)
    }

    private fun move(scanCode: Int, action: Int) {
        if (this.state == State.STOPPED
            && scanCode == OnKeyListener.Button.ARROW_RIGHT.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            stepRight()
        } else if (this.state == State.STOPPED
            && scanCode == OnKeyListener.Button.ARROW_LEFT.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            stepLeft()
        } else if (this.state == State.STOPPED
            && scanCode == OnKeyListener.Button.ARROW_DOWN.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            stepDown()
        } else if (this.state == State.STOPPED
            && scanCode == OnKeyListener.Button.ARROW_UP.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            stepUp()
        }
    }

    private fun configureEyes(scanCode: Int, action: Int) {
        if (!lookingRight
            && scanCode == OnKeyListener.Button.ARROW_RIGHT.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            lookRight()
        } else if (lookingRight
            && scanCode == OnKeyListener.Button.ARROW_LEFT.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            lookLeft()
        } else if (!lookingFront && scanCode == OnKeyListener.Button.ARROW_DOWN.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            lookFront()
        } else if (!lookingBack && scanCode == OnKeyListener.Button.ARROW_UP.code
            && OnKeyListener.Action.downOrPressed(action)
        ) {
            lookBack()
        }
    }

    private fun stopLookingVertical() {
        eye1.color = Engine.Color(darkerGrayColorBack, 1f)
        eye2.color = Engine.Color(darkerGrayColorBack, 1f)
        if (lookingRight) {
            lookRight()
        } else {
            lookLeft()
        }
        lookingFront = false
        lookingBack = false
    }

    private fun lookLeft() {
        lookingRight = false
        rightLegFirst = false
        eye1.color = Engine.Color(darkerGrayColorBack, 0f)
        eye2.color = Engine.Color(darkerGrayColorBack, 1f)
    }

    private fun lookRight() {
        lookingRight = true
        rightLegFirst = true
        eye1.color = Engine.Color(darkerGrayColorBack, 1f)
        eye2.color = Engine.Color(darkerGrayColorBack, 0f)
    }

    private fun lookFront() {
        eye1.color = Engine.Color(darkerGrayColorBack, 1f)
        eye2.color = Engine.Color(darkerGrayColorBack, 1f)
        lookingFront = true
        lookingBack = false
        rightLegFirst = true
    }

    private fun lookBack() {
        eye1.color = Engine.Color(darkerGrayColorBack, 0f)
        eye2.color = Engine.Color(darkerGrayColorBack, 0f)
        lookingFront = false
        lookingBack = true
        rightLegFirst = true
    }

    private fun stepRight() {
        this.state = State.WALKING
        if (rightLegFirst) {
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightLeg.apply(Rotation(x, y + LEG_HEIGHT, Math.PI / 4))
        } else {
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            leftLeg.apply(Rotation(x, y + LEG_HEIGHT, Math.PI / 4))
        }
        finishMoving()
    }

    private fun stepLeft() {
        this.state = State.WALKING
        if (rightLegFirst) {
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightLeg.apply(Rotation(x, y + LEG_HEIGHT, -Math.PI / 4))
        } else {
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            leftLeg.apply(Rotation(x, y + LEG_HEIGHT, -Math.PI / 4))
        }
        finishMoving()
    }

    private fun stepDown() {
        this.state = State.WALKING
        if (rightLegFirst) {
            val (rx, ry) = rightLeg.getPrimitive().getCenter()
            rightLeg.apply(Translation(rx, ry) + Scale(0.9f, 0.9f) + Translation(-rx, -ry))
        } else {
            val (rx, ry) = leftLeg.getPrimitive().getCenter()
            leftLeg.apply(Translation(rx, ry) + Scale(0.9f, 0.9f) + Translation(-rx, -ry))
        }
        finishMoving()
    }

    private fun stepUp() {
        this.state = State.WALKING
        finishMoving()
    }

    private fun finishMoving() {
        val timer = Timer()
        timer.schedule(timerTask {
            if (state == State.WALKING && !(lookingFront || lookingBack) && lookingRight) {
                moveRight()
            } else if (state == State.WALKING && !(lookingFront || lookingBack) && !lookingRight) {
                moveLeft()
            } else if (state == State.WALKING && lookingFront) {
                moveFront()
            } else if (state == State.WALKING && lookingBack) {
                moveBack()
            }
        }, 100)

    }

    private fun moveRight() {
        this.state = State.MOVED
        if (rightLegFirst) {
            rightLegFirst = false
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightLeg.apply(Rotation(x, y + LEG_HEIGHT, -Math.PI / 4))
        } else {
            rightLegFirst = true
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            leftLeg.apply(Rotation(x, y + LEG_HEIGHT, -Math.PI / 4))
        }
        primitives.forEach {
            it.apply(Translation(STEP_DISTANCE, 0f))
        }
    }

    private fun moveLeft() {
        this.state = State.MOVED
        if (rightLegFirst) {
            rightLegFirst = false
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightLeg.apply(Rotation(x, y + LEG_HEIGHT, Math.PI / 4))
        } else {
            rightLegFirst = true
            leftArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, -Math.PI / 4))
            rightArm.apply(Rotation(x, y + LEG_HEIGHT + BODY_HEIGHT, Math.PI / 4))
            leftLeg.apply(Rotation(x, y + LEG_HEIGHT, Math.PI / 4))
        }
        primitives.forEach {
            it.apply(Translation(-STEP_DISTANCE, 0f))
        }
    }

    private fun moveFront() {
        this.state = State.MOVED
        if (rightLegFirst) {
            rightLegFirst = false
            val (rx, ry) = rightLeg.getPrimitive().getCenter()
            rightLeg.apply(Translation(rx, ry) + Scale(1.1f, 1.1f) + Translation(-rx, -ry))
        } else {
            rightLegFirst = true
            val (rx, ry) = leftLeg.getPrimitive().getCenter()
            leftLeg.apply(Translation(rx, ry) + Scale(1.1f, 1.1f) + Translation(-rx, -ry))
        }
        val (x, y) = getCenter()
        primitives.forEach {
            it.apply(Translation(0f, -STEP_DISTANCE))
            it.apply(
                Translation(x, y) + Scale(
                    1f + STEP_DISTANCE * abs(y),
                    1f + STEP_DISTANCE * abs(y)
                ) + Translation(-x, -y)
            )
        }
    }

    private fun moveBack() {
        this.state = State.MOVED
        val (x, y) = getCenter()
        primitives.forEach {
            it.apply(Translation(0f, STEP_DISTANCE))
            it.apply(
                Translation(x, y) + Scale(
                    1f - STEP_DISTANCE * abs(y),
                    1f - STEP_DISTANCE * abs(y)
                ) + Translation(-x, -y)
            )
        }
    }

    private fun getCenter(): Pair<Float, Float> {
        val maxX = rightLeg.getPrimitive().getCenter().first + LEG_WIDTH / 2
        val maxY = head.getPrimitive().getCenter().second + HEAD_RADIUS
        val minX = leftLeg.getPrimitive().getCenter().first - LEG_WIDTH / 2
        val minY = leftLeg.getPrimitive().getCenter().second - LEG_HEIGHT / 2

        return Pair((maxX + minX) / 2, (maxY + minY) / 2)
    }

}
