package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.primitives.Square

/**
 * Class that represents the background sky scenario
 *
 * It's composed by one single primitive:
 * - [Square] for the sky;
 *
 */
class Sky : Drawable {
    companion object {
        private val blueColor = Engine.Color(193, 228, 236)
    }

    private val square = Square(
        floatArrayOf(
            -1f, 1f,
            1f, 1f,
            -1f, -0.7f,
            1f, -0.7f
        )
    )

    private val cloud = Cloud(-0.5f, 0.71f)
    private val cloud2 = Cloud(0.2f, 0.31f, 0.6f)

    private val primitives = listOf(square, cloud, cloud2)

    init {
        square.color = blueColor
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

}
