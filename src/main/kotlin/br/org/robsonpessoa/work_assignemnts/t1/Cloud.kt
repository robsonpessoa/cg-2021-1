package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnTimeTickListener
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.transformation.MutablePrimitive
import br.org.robsonpessoa.transformation.Scale
import br.org.robsonpessoa.transformation.Translation
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class Cloud(val x: Float, val y: Float, scale: Float? = null) : Drawable, OnTimeTickListener {
    companion object {
        private val whiteColor = Engine.Color(230, 230, 230)
    }

    private var direction = 1

    private val circle1 = MutablePrimitive(Circle(0.08f, x - 0.1f, y - 0.01f))
    private val circle2 = MutablePrimitive(Circle(0.08f, x, y + 0.04f))
    private val circle3 = MutablePrimitive(Circle(0.08f, x, y - 0.04f))
    private val circle4 = MutablePrimitive(Circle(0.08f, x + 0.1f, y))

    private val primitives = listOf(circle1, circle2, circle3, circle4)

    init {
        circle1.color = whiteColor
        circle2.color = whiteColor
        circle3.color = whiteColor
        circle4.color = whiteColor

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate({
            this.onTimeTick()
        }, 0, 1300, TimeUnit.MILLISECONDS)

        scale?.let { s ->
            primitives.forEach {
                it.apply(Translation(x, y) + Scale(s, s) + Translation(-x, -y))
            }
        }
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

    override fun onTimeTick() {
        primitives.forEach {
            it.apply(Translation(0.05f * direction, 0.02f * direction))
        }
        direction *= -1;
    }
}
