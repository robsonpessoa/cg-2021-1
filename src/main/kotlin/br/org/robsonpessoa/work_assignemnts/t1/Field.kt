package br.org.robsonpessoa.work_assignemnts.t1

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.primitives.Circle
import br.org.robsonpessoa.primitives.Square

/**
 * Class that represents the background field scenario
 *
 * It's composed by some primitives:
 * - [Square] for the floor,
 * - [Circle] for the two mountains,
 * - [Flower] for visual enhancement with flowers;
 *
 */
class Field : Drawable {
    companion object {
        private val lightGreenColor = Engine.Color(78, 158, 18)
        private val darkGreenColor = Engine.Color(79, 119, 49)
        private val darkerGreenColor = Engine.Color(113, 162, 112)
    }

    private val square = Square(
        floatArrayOf(
            -1f, -1f,
            1f, -1f,
            -1f, -0.7f,
            1f, -0.7f
        )
    )

    private val mountain1 = Circle(
        0.9f, 0.9f, -1f
    )

    private val mountain2 = Circle(
        1.5f, -0.9f, -1f
    )

    private val flower1 = Flower(0.7f, -0.4f, Engine.Color(150, 0, 0))
    private val flower2 = Flower(0.8f, -0.5f, Engine.Color(150, 0, 0))
    private val flower3 = Flower(0.76f, -0.3f, Engine.Color(150, 0, 0))

    private val flower4 = Flower(0.1f, -0.9f, Engine.Color(0, 0, 180))
    private val flower5 = Flower(-0.9f, -0.87f, Engine.Color(0, 0, 180))
    private val flower6 = Flower(0.8f, -0.84f, Engine.Color(0, 0, 180))

    private val primitives = listOf(mountain2, mountain1, flower1, flower2, flower3, square, flower4, flower5, flower6)

    init {
        square.color = lightGreenColor
        mountain1.color = darkGreenColor
        mountain2.color = darkerGreenColor
    }

    override fun draw(engine: Engine) {
        primitives.forEach {
            it.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        primitives.forEach {
            it.configureVertices(builder)
        }
    }

}
