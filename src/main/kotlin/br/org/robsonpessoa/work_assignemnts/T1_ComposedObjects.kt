package br.org.robsonpessoa.work_assignemnts

import br.org.robsonpessoa.glfw.Application
import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.OnKeyListener
import br.org.robsonpessoa.glsl.FragmentShader
import br.org.robsonpessoa.glsl.VertexShader
import br.org.robsonpessoa.work_assignemnts.t1.Field
import br.org.robsonpessoa.work_assignemnts.t1.Person
import br.org.robsonpessoa.work_assignemnts.t1.Sky
import br.org.robsonpessoa.work_assignemnts.t1.Sun

/**
 * Work assignment 1 of the Computer Graphics discipline SCC0250 at
 * Instituto de Ciências Matemáticas e Computação (ICMC/USP)
 * of the Universidade de São Paulo (USP).
 *
 * @author Robson Pessoa <robsonpessoa@usp.br> - 8632563
 *
 */
class WorkAssignment1(private val engine: Engine) : Engine.EngineListener, OnKeyListener {

    private val sun = Sun()
    private val sky = Sky()
    private val field = Field()
    private val person = Person(-0.9f, -0.71f)

    private val objects = listOf(sky, sun, field, person)

    override fun onBuildProgram(builder: ApplicationBuilder) {
        configureShaders(builder)
        configureVertices(builder)
    }

    override fun onProgramStarted(program: Application) {

    }

    override fun onDraw() {
        objects.forEach {
            it.draw(engine)
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        objects.forEach {
            it.configureVertices(builder)
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        person.onKeyPressed(key, scanCode, action, mods)
    }
}
