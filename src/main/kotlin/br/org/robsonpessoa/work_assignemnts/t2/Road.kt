package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Road : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mutableMapOf(Pair("Materiais.002", "textures/road/road.jpg")),
        WavefrontObjectFactory.create("textures/road/road.obj"),
        fallback = "textures/road/road.jpg"
    )

    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
