package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.matrix.IlluminationController
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithCoordinates
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Sky : Drawable, Textured, Normalized {

    private val movementSpeed = 5
    private var angle = 0

    private val sun = Sun()
    private val moon = Moon()

    private val distanceFactor = IntRange(0, 360).associateWith {
        val relativeAngle = it % 180
        -kotlin.math.abs(90 - relativeAngle) / 180
    }

    override val obj = WithTexture(
        mutableMapOf(Pair("Materiais", "textures/sky/sky.jpg")),
        WavefrontObjectFactory.create(
            "textures/sky/sky.obj",
            ambientCoefficient = 0.8f,
            diffuseCoefficient = 0.5f,
            specularCoefficient = 0f
        ),
    )

    override fun draw(engine: Engine) {
        val objAmbianceFactor = if (angle in 0..179) {
            focusLightOn(engine, sun)
            sun.draw(engine)
            sun.move(angle)
            0.1f
        } else {
            focusLightOn(engine, moon)
            moon.draw(engine)
            moon.move(angle)
            -0.2f
        }

        IlluminationController.setAmbientLuminanceFactor(-distanceFactor[angle]!! + objAmbianceFactor)

        obj.draw(engine)

        angle = (angle + movementSpeed) % 360
    }

    private fun focusLightOn(engine: Engine, obj: WithCoordinates<Triple<Float, Float, Float>>) {
        val coords = obj.getCenter()
        IlluminationController.refreshIlluminationFont(
            engine.getApplication()!!.id,
            coords.first,
            coords.second,
            coords.third
        )
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)

        sun.configureVertices(builder)
        moon.configureVertices(builder)
    }

    override fun configureTextures(builder: ApplicationBuilder) {
        obj.configureTextures(builder)

        sun.configureTextures(builder)
        moon.configureTextures(builder)
    }

    override fun configureNormals(builder: ApplicationBuilder) {
        obj.configureNormals(builder)

        sun.configureNormals(builder)
        moon.configureNormals(builder)
    }
}
