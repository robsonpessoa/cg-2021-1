package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Dog : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mutableMapOf(),
        WavefrontObjectFactory.create("textures/dog/dog.obj"),
        fallback = "textures/dog/Australian_Cattle_Dog_dif.jpg"
    )
    
    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
