package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Floor : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mutableMapOf(
            Pair("Material.001_BrickRound0108_5_S.jpg", "textures/floor/BrickRound0108_5_S.jpg")
        ),
        WavefrontObjectFactory.create("textures/floor/floor.obj")
    )

    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
