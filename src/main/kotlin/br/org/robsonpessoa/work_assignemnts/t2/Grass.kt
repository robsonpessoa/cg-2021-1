package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Grass : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mapOf(
            Pair("10450_Rectangular_Grass_Patch_v1.001", "textures/ground/grass.jpg")
        ),
        WavefrontObjectFactory.create("textures/ground/grass.obj")
    )

    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
