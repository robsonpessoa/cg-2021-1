package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class Box : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mutableMapOf(Pair("caixa2", "textures/box/caixa2.jpg")),
        WavefrontObjectFactory.create("textures/box/caixa.obj")
    )

    override fun draw(engine: Engine) {
        obj.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
