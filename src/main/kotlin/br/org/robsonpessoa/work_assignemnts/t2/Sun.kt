package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.glfw.Normalizable
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithCoordinates
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory
import org.joml.Matrix4f
import org.joml.Vector4f

class Sun : Drawable, Textured, Normalizable, WithCoordinates<Triple<Float, Float, Float>> {

    private var angle = 0

    override val obj = WithTexture(
        mutableMapOf(Pair("13913_Sun", "textures/sun/sun.jpg")),
        WavefrontObjectFactory.create(
            "textures/sun/sun.obj",
            diffuseCoefficient = 0.8f,
            ambientCoefficient = 0.8f,
            specularCoefficient = 0.8f
        )
    )

    private val center = this.obj.getCenter().let { Vector4f(it.first, it.second, it.third, 1f) }

    private val positions = IntRange(0, 180).associateWith {
        val matrixTransform = Matrix4f().rotation(Math.toRadians(it.toDouble()).toFloat(), 1f, 0f, 1f)
        val centerMoved = Vector4f()
        center.mul(matrixTransform, centerMoved)
        Pair(matrixTransform, centerMoved)
    }

    override fun draw(engine: Engine) {
        Model(positions[angle]!!.first).use(engine.getApplication()!!.id) {
            obj.draw(engine)
        }
    }

    fun move(angle: Int) {
        this.angle = angle
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }

    override fun configureNormals(builder: ApplicationBuilder) {
        obj.configureNormals(builder)
    }

    override fun getCenter(): Triple<Float, Float, Float> {
        return Triple(
            positions[angle]!!.second.x,
            positions[angle]!!.second.y,
            positions[angle]!!.second.z
        )
    }
}
