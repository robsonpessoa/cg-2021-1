package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory
import org.joml.Matrix4f

class Tree(val x: Float, val y: Float, val z: Float) : Drawable, Textured, Normalized {

    override val obj = WithTexture(
        mutableMapOf(
            Pair("polySurface1SG1", "textures/tree/DB2X2_L01.png"),
            Pair("Trank_bark", "textures/tree/bark_0021.jpg")
        ),
        WavefrontObjectFactory.create(
            "textures/tree/tree.obj"
        ),
        flipVertically = true
    )

    override fun draw(engine: Engine) {
        var matrixTransform = Matrix4f()
        matrixTransform = matrixTransform.translate(x, y, z)

        Model(matrixTransform).use(engine.getApplication()!!.id) {
            obj.draw(engine)
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
