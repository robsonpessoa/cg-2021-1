package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory
import org.joml.Matrix4f

class Car : Drawable, Textured, Normalized {

    private var currentStep: Int = 0
    private val speed = 1

    override val obj = WithTexture(
        mutableMapOf(Pair("CAR_05.001", "textures/car/Car_05.png")),
        WavefrontObjectFactory.create(
            "textures/car/car.obj"
        )
    )

    private val positions = IntRange(0, 40).associateWith {
        Matrix4f().translate(it - 5f, 0f, 0f)
    }

    override fun draw(engine: Engine) {
        Model(positions[currentStep]!!).use(engine.getApplication()!!.id) {
            obj.draw(engine)
        }

        if (currentStep == 40) {
            currentStep = 0
        }

        currentStep += speed
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)
    }
}
