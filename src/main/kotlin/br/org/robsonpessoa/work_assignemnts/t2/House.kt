package br.org.robsonpessoa.work_assignemnts.t2

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.wavefront.WavefrontObjectFactory

class House : Drawable, Textured, Normalized {

    private val floor = Floor()
    private val cat = Cat()
    private val table = Table()
    private val fireplace = Fireplace()

    override val obj = WithTexture(
        mapOf(
            Pair("cottage_texture", "textures/house/cottage_texture.png")
        ),
        WavefrontObjectFactory.create(
            "textures/house/house.obj",
            ambientCoefficient = 0.6f,
            diffuseCoefficient = 0.5f
        )
    )

    override fun draw(engine: Engine) {
        obj.draw(engine)

        floor.draw(engine)
        cat.draw(engine)
        fireplace.draw(engine)
        table.draw(engine)
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        obj.configureVertices(builder)

        floor.configureVertices(builder)
        cat.configureVertices(builder)
        fireplace.configureVertices(builder)
        table.configureVertices(builder)
    }

    override fun configureTextures(builder: ApplicationBuilder) {
        obj.configureTextures(builder)

        floor.configureTextures(builder)
        cat.configureTextures(builder)
        fireplace.configureTextures(builder)
        table.configureTextures(builder)
    }

    override fun configureNormals(builder: ApplicationBuilder) {
        obj.configureNormals(builder)

        floor.configureNormals(builder)
        cat.configureNormals(builder)
        fireplace.configureNormals(builder)
        table.configureNormals(builder)
    }
}
