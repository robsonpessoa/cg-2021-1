package br.org.robsonpessoa.work_assignemnts

import br.org.robsonpessoa.glfw.*
import br.org.robsonpessoa.glsl.FragmentShader
import br.org.robsonpessoa.glsl.VertexShader
import br.org.robsonpessoa.matrix.IlluminationController
import br.org.robsonpessoa.matrix.Projection
import br.org.robsonpessoa.matrix.View
import br.org.robsonpessoa.model.Normalized
import br.org.robsonpessoa.model.Textured
import br.org.robsonpessoa.work_assignemnts.t2.*
import org.lwjgl.opengl.GL11.*

/**
 * Work assignment 2 of the Computer Graphics discipline SCC0250 at
 * Instituto de Ciências Matemáticas e Computação (ICMC/USP)
 * of the Universidade de São Paulo (USP).
 *
 * @author Robson Pessoa <robsonpessoa@usp.br> - 8632563
 *
 */
class WorkAssignment2(private val engine: Engine) : Engine.EngineListener, OnKeyListener, OnMouseListener {

    private var polygonalMode = false

    private val view = View(
        cameraPosition = floatArrayOf(-16f, 3f, -5f),
        cameraFront = floatArrayOf(1f, 0f, 0f),
        cameraUp = floatArrayOf(0f, 1f, 0f)
    )

    private val projection = lazy {
        Projection(
            engine.getWindow()!!.height,
            engine.getWindow()!!.width
        )
    }

    private val sky = Sky()

    private val box = Box()
    private val car = Car()
    private val grass = Grass()
    private val road = Road()
    private val dog = Dog()

    private val house = House()

    private val tree1 = Tree(0f, 0f, 0f)
    private val tree2 = Tree(1f, 0f, -1f)
    private val tree3 = Tree(-1f, 0f, -2.2f)
    private val tree4 = Tree(-2f, 0f, -1.2f)
    private val tree5 = Tree(0f, 0f, -4f)
    private val tree6 = Tree(1f, 0f, -4.7f)
    private val tree7 = Tree(-1f, 0f, -4.2f)
    private val tree8 = Tree(-2f, 0f, -5.2f)

    private val objects = listOf(
        house, sky,
        car, box, dog, grass, road,
        tree1, tree2, tree3, tree4,
        tree5, tree6, tree7, tree8
    )

    override fun onBuildProgram(builder: ApplicationBuilder) {
        builder.setDimension(3)

        configureShaders(builder)
        configureVertices(builder)
        configureTextures(builder)
        configureNormals(builder)

        IlluminationController.setView(view)
    }

    override fun onProgramStarted(program: Application) {
        projection.value.apply(program.id)
        view.init(engine.getWindow()!!)
    }

    override fun onDraw() {
        val programId = engine.getApplication()!!.id

        if (polygonalMode) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
            engine.showAxes()
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
            engine.hideAxes()
        }

        view.refresh(programId)

        objects.forEach {
            if (it is Drawable) {
                it.draw(engine)
            }
        }
    }

    private fun configureVertices(builder: ApplicationBuilder) {
        objects.forEach {
            if (it is Drawable) {
                it.configureVertices(builder)
            }
        }
    }

    private fun configureTextures(builder: ApplicationBuilder) {
        objects.forEach {
            if (it is Textured) {
                it.configureTextures(builder)
            }
        }
    }

    private fun configureNormals(builder: ApplicationBuilder) {
        objects.forEach {
            if (it is Normalized) {
                it.configureNormals(builder)
            }
        }
    }

    private fun configureShaders(builder: ApplicationBuilder) {
        builder.addShader(
            VertexShader()
        )

        builder.addShader(
            FragmentShader()
        )
    }

    override fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int) {
        val cameraSpeed = 0.5f
        if (key == 87 && (action == 1 || action == 2)) {
            view.zoomIn(cameraSpeed)
        }

        if (key == 83 && (action == 1 || action == 2)) {
            view.zoomOut(cameraSpeed)
        }

        if (key == 65 && (action == 1 || action == 2)) {
            view.left(cameraSpeed)
        }

        if (key == 68 && (action == 1 || action == 2)) {
            view.right(cameraSpeed)
        }

        if (key == 80 && action == 1) {
            polygonalMode = !polygonalMode
        }

        if (key == 265 && action == 1) {
            IlluminationController.increaseKa()
        }

        if (key == 264 && action == 1) {
            IlluminationController.decreaseKa()
        }

        if (key == 263 && action == 1) {
            IlluminationController.decreaseKd()
        }

        if (key == 262 && action == 1) {
            IlluminationController.increaseKd()
        }
    }

    override fun onMouseClick(button: Int, action: Int, mods: Int) {

    }

    override fun onMouseMove(x: Float, y: Float) {
        view.look(x, y)
    }
}
