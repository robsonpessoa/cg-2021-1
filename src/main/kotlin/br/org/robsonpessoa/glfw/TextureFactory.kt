package br.org.robsonpessoa.glfw

import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL30.*
import java.awt.Color
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import java.nio.ByteBuffer
import javax.imageio.ImageIO


class TextureFactory(private val filename: String) {

    private fun loadImage(): BufferedImage? {
        ClassLoader.getSystemResourceAsStream(filename).use { inputStream ->
            inputStream?.let {
                return ImageIO.read(it)
            }
        }
        return null
    }

    private fun BufferedImage.flipVertically(): BufferedImage {
        val at = AffineTransform()
        at.concatenate(AffineTransform.getScaleInstance(1.0, -1.0))
        at.concatenate(AffineTransform.getTranslateInstance(0.0, -height * 1.0))
        return applyTransformation(at)
    }

    private fun BufferedImage.flipHorizontally(): BufferedImage {
        val at = AffineTransform()
        at.concatenate(AffineTransform.getScaleInstance(-1.0, 1.0))
        at.concatenate(AffineTransform.getTranslateInstance(-width * 1.0, 0.0))
        return applyTransformation(at)
    }

    private fun BufferedImage.flip(): BufferedImage {
        val at = AffineTransform()
        at.concatenate(AffineTransform.getScaleInstance(-1.0, -1.0))
        at.concatenate(AffineTransform.getTranslateInstance(-width * 1.0, -height * 1.0))
        return applyTransformation(at)
    }

    private fun BufferedImage.applyTransformation(at: AffineTransform): BufferedImage {
        val newImage = BufferedImage(
            width, height,
            BufferedImage.TYPE_INT_ARGB
        )
        val g = newImage.createGraphics()
        g.transform(at)
        g.drawImage(this, 0, 0, null)
        g.dispose()
        return newImage
    }

    private fun BufferedImage.toBuffer(): ByteBuffer {
        val buffer: ByteBuffer = BufferUtils.createByteBuffer(width * height * 4)
        for (h in 0 until height) {
            for (w in 0 until width) {
                val color = Color(getRGB(w, h))
                buffer.put((color.red).toByte())
                buffer.put((color.green).toByte())
                buffer.put((color.blue).toByte())
                buffer.put((color.alpha).toByte())
            }
        }
        return buffer.rewind().asReadOnlyBuffer()
    }

    fun create(flipVertically: Boolean, flipHorizontally: Boolean): Int? {
        var id: Int? = null

        if (ClassLoader.getSystemResource(filename) != null) {
            id = glGenTextures()
            loadImage()?.let { bindTexture(id, flipVertically, flipHorizontally, it) }
        }
        return id
    }

    private fun bindTexture(
        id: Int,
        flipVertically: Boolean,
        flipHorizontally: Boolean,
        image: BufferedImage
    ) {
        glBindTexture(GL_TEXTURE_2D, id)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        val buffer = when {
            flipVertically && !flipHorizontally -> image.flipVertically()
            !flipVertically && flipHorizontally -> image.flipHorizontally()
            flipVertically && flipHorizontally -> image.flip()
            else -> image
        }.toBuffer()

        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, buffer
        )

        glGenerateMipmap(GL_TEXTURE_2D)
    }
}
