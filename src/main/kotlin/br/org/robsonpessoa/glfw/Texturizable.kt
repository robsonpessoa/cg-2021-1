package br.org.robsonpessoa.glfw

interface Texturizable {
    fun configureTextures(builder: ApplicationBuilder)
}
