package br.org.robsonpessoa.glfw

interface Drawable {
    fun draw(engine: Engine)
    fun configureVertices(builder: ApplicationBuilder)
}
