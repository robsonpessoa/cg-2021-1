/***
 * The first version was a copy of the class Engine of https://github.com/Zi0P4tch0/LWJGL-Kotlin-Example
 *
 * Some modifications were made to accomplish better decoupling of functions.
 */

package br.org.robsonpessoa.glfw

import br.org.robsonpessoa.math.IdentityMatrix
import br.org.robsonpessoa.math.toBuffer
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.matrix.Projection
import br.org.robsonpessoa.matrix.View
import br.org.robsonpessoa.utils.merge
import org.joml.Matrix4f
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL20.*

/**
 * A class that represents the engine that makes graphical computation using OpenGL libraries.
 *
 */
class Engine(
    val backgroundColor: Color = Color(0f, 0f, 0f),
    val foregroundColor: Color = Color(0f, 0f, 0f, 0f)
) {

    private var engineListener: EngineListener? = null

    private var application: Application? = null
    private var window: Window? = null

    private var axesList: Int? = null

    private var axesShown = false

    /**
     * Starts the engine configuration.
     *
     */
    private fun init() {
        if (window == null) {
            throw java.lang.IllegalStateException("The Engine must have its window configured before it starts.")
        }

        window!!.init()

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities()

        configureGLSLProgram()

        // Set the clear color
        glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, 1.0f)

        glEnable(GL_POLYGON_OFFSET_LINE)

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

        createAxesLines()

        clearModel()
    }

    private fun createAxesLines() {
        /* Create a display list for drawing axes */
        axesList = glGenLists(1)
        glNewList(axesList!!, GL_COMPILE)

        glBegin(GL_LINE_STRIP)
        glVertex3f(0.0f, 0.0f, 0.0f)
        glVertex3f(200.0f, 0.0f, 0.0f)
        glVertex3f(-200.0f, 0.0f, 0.0f)
        glEnd()
        glBegin(GL_LINE_STRIP)
        glVertex3f(0.0f, 0.0f, 0.0f)
        glVertex3f(0.0f, 200.0f, 0.0f)
        glVertex3f(0.0f, -200.0f, 0.0f)
        glEnd()
        glBegin(GL_LINE_STRIP)
        glVertex3f(0.0f, 0.0f, 0.0f)
        glVertex3f(0.0f, 0.0f, 200.0f)
        glVertex3f(0.0f, 0.0f, -200.0f)
        glEnd()

        glEndList()
    }

    fun showAxes() {
        axesShown = true
    }

    fun hideAxes() {
        axesShown = false
    }

    /**
     * Configures the GLSL Program according to the [EngineListener] applied to the object.
     *
     */
    private fun configureGLSLProgram() {
        val builder = ApplicationBuilder()
        engineListener?.onBuildProgram(builder)
        application = builder.apply()
        engineListener?.onProgramStarted(application!!)
    }

    /**
     * Loop and print the objects configured while the Window created is open.
     *
     */
    private fun loop() {
        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (window!!.isOpen()) {
            // Clear the framebuffer
            glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
            glClearColor(backgroundColor.red, backgroundColor.green, backgroundColor.blue, 1.0f)

            engineListener?.onDraw()

            if (axesShown) {
                glColor4ui(0, 0, 255, 255)
                glPushMatrix()
                glCallList(axesList!!)
                glPopMatrix()
                glColor4f(foregroundColor.red, foregroundColor.green, foregroundColor.blue, 1f)
            }

            window!!.sync()
        }
    }

    private fun clearModel() {
        getApplication()?.let {
            val loc = GL20.glGetUniformLocation(
                getApplication()!!.id,
                Model.VERTEX_NAME
            )

            GL20.glUniformMatrix4fv(
                loc, true,
                Matrix4f().toBuffer()
            )
        }
    }

    private fun clearView() {
        getApplication()?.let {
            val loc = GL20.glGetUniformLocation(
                getApplication()!!.id,
                View.VERTEX_NAME
            )

            GL20.glUniformMatrix4fv(
                loc, false,
                IdentityMatrix(4).toBuffer()
            )
        }
    }

    private fun clearProjection() {
        getApplication()?.let {
            val loc = GL20.glGetUniformLocation(
                getApplication()!!.id,
                Projection.VERTEX_NAME
            )

            GL20.glUniformMatrix4fv(
                loc, false,
                IdentityMatrix(4).toBuffer()
            )
        }
    }

    /**
     * Starts the engine.
     *
     */
    fun run() {
        GLFWModule.use {
            init()
            loop()

            window!!.destroy()

            clearModel()
            clearView()
            clearProjection()

            glDisable(GL_POLYGON_OFFSET_LINE)
        }
    }

    /**
     * Creates a [Window].
     *
     * @param title the window title.
     * @param height the window height.
     * @param width the window width.
     */
    fun setWindowConfiguration(title: String, height: Int, width: Int) {
        window = Window(title, width, height)
    }

    /**
     * Gets the configured window.
     *
     * @return the [Window].
     */
    fun getWindow(): Window? = window

    /**
     * Gets the configured GLSL Application.
     *
     * @return the [Application].
     */
    fun getApplication(): Application? = application

    /**
     * Setter of the Engine Listener.
     *
     * @param listener the [EngineListener].
     */
    fun setOnEngineListener(listener: EngineListener) {
        this.engineListener = listener
    }

    /**
     * Interface that all the listeners of the [Engine] must implement.
     *
     */
    interface EngineListener {
        /**
         * Called when the GLSL Program is being built.
         *
         * @param builder the [ApplicationBuilder].
         */
        fun onBuildProgram(builder: ApplicationBuilder)

        /**
         * Called when the GLSL Program is being built.
         *
         * @param builder the [ApplicationBuilder].
         */
        fun onProgramStarted(program: Application)

        /**
         * Called in all [loop] iteration to draw objects according to the program built configuration.
         *
         */
        fun onDraw()
    }

    data class Color(val red: Float, val green: Float, val blue: Float, val alpha: Float = 1f) {
        constructor(red: Int, green: Int, blue: Int, alpha: Float = 1f) : this(
            red / 255f,
            green / 255f,
            blue / 255f,
            alpha
        )

        constructor(color: Color, alpha: Float = 1f) : this(color.red, color.green, color.blue, alpha)

        fun toFloatArray(times: Int = 1): FloatArray =
            (0 until times).map { floatArrayOf(red, green, blue, alpha) }.merge()
    }

}
