package br.org.robsonpessoa.glfw

import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.primitives.Primitive
import br.org.robsonpessoa.utils.merge
import br.org.robsonpessoa.utils.toBuffer
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL15
import org.lwjgl.opengl.GL20

/**
 * Represents the application and it's configuration.
 *
 * @property id the id of the OpenGL program object.
 * @property shaders a list of the configured [Shader]'s.
 * @property vertices a map <name, data> of the configured data matrices.
 * @property textures a map <name, data> of the configured textures.
 * @property normals a map <name, data> of the configured normals.
 * @property dimensions the number of axis to be used in the application.
 */
data class Application(
    val id: Int,
    val shaders: List<Shader>,
    private val vertices: Map<String, FloatArray>,
    private val textures: Map<String, FloatArray>,
    private val normals: Map<String, FloatArray>,
    private val dimensions: Int
) {
    data class ObjectVertexData(val start: Int, val length: Int)

    private val objectsAtVerticesArray = mutableMapOf<String, ObjectVertexData>()

    operator fun get(name: String): ObjectVertexData? = objectsAtVerticesArray[name]

    init {
        configureShaders()

        val buffer = IntArray(3)
        GL15.glGenBuffers(buffer)
        configureVertices(buffer[0], dimensions)
        configureTextures(buffer[1])
        configureNormals(buffer[2])

        GL20.glUseProgram(id)

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        if (dimensions > 2) {
            glEnable(GL_DEPTH_TEST)
        }
    }

    private fun Map<String, FloatArray>.toFloatArray(): FloatArray {
        return values.merge()
    }

    private fun configureShaders() {
        shaders.forEach { shader ->
            shader.attach(this)
        }
        link()
    }

    private fun configureVertices(buffer: Int, dimensions: Int) {
        val fullData = vertices.toFloatArray()

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fullData.toBuffer(), GL15.GL_STATIC_DRAW)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)

        val position = GL20.glGetAttribLocation(this.id, Primitive.VERTEX_NAME)
        GL20.glEnableVertexAttribArray(position)

        // FIXME When using the pointer variable, it doesn't work
        GL20.glVertexAttribPointer(
            position, dimensions, GL11.GL_FLOAT,
            false, getVerticesDataStrideBytes(dimensions), 0
        )

        mapObjectsLocation(dimensions)
    }

    private fun mapObjectsLocation(dimensions: Int) {
        var offset = 0L
        vertices.forEach { (key, values) ->
            val step = values.size / dimensions
            objectsAtVerticesArray[key] = ObjectVertexData(
                start = offset.toInt(),
                length = step
            )

            offset += step
        }
    }

    private fun configureTextures(buffer: Int) {
        val fullData = textures.toFloatArray()

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fullData.toBuffer(), GL15.GL_STATIC_DRAW)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)

        val position = GL20.glGetAttribLocation(this.id, WithTexture.VERTEX_NAME)
        GL20.glEnableVertexAttribArray(position)

        GL20.glVertexAttribPointer(
            position, 2, GL11.GL_FLOAT,
            false, getVerticesDataStrideBytes(2), 0
        )
    }

    private fun configureNormals(buffer: Int) {
        val fullData = normals.toFloatArray()

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fullData.toBuffer(), GL15.GL_STATIC_DRAW)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buffer)

        val position = GL20.glGetAttribLocation(this.id, "normal")
        GL20.glEnableVertexAttribArray(position)

        GL20.glVertexAttribPointer(
            position, 3, GL11.GL_FLOAT,
            false, getVerticesDataStrideBytes(2), 0
        )
    }

    private fun link() {
        GL20.glLinkProgram(id)

        val buffer = IntArray(1)
        GL20.glGetProgramiv(id, GL20.GL_LINK_STATUS, buffer)
        if (buffer[0] != GL11.GL_TRUE) {
            val error = GL20.glGetProgramInfoLog(id)
            throw RuntimeException("Error while linking the GLSL file: $error")
        }
    }

    companion object {
        private const val FLOAT_SIZE_BYTES = 4
        private fun getVerticesDataStrideBytes(dimensions: Int) = dimensions * FLOAT_SIZE_BYTES
    }
}
