package br.org.robsonpessoa.glfw

interface OnMouseListener {
    fun onMouseClick(button: Int, action: Int, mods: Int)
    fun onMouseMove(x: Float, y: Float)
}
