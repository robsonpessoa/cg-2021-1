package br.org.robsonpessoa.glfw

interface OnKeyListener {
    fun onKeyPressed(key: Int, scanCode: Int, action: Int, mods: Int)

    enum class Action(val code: Int) {
        DOWN(0),
        UP(1),
        PRESSED(2);

        fun downOrPressed(code: Int): Boolean {
            return code in arrayOf(DOWN.code, PRESSED.code)
        }

        companion object {
            fun downOrPressed(code: Int): Boolean {
                return code in arrayOf(DOWN.code, PRESSED.code)
            }
        }
    }

    enum class Button(val code: Int) {
        W(25),
        A(38),
        S(39),
        D(40),

        ARROW_UP(111),
        ARROW_RIGHT(114),
        ARROW_LEFT(113),
        ARROW_DOWN(116)
    }
}
