package br.org.robsonpessoa.glfw

import org.lwjgl.opengl.GL20.glCreateProgram

/**
 * A class that represents the settings of the program related of shaders and data matrix'es to be applied.
 *
 */
class ApplicationBuilder {
    private val shaders: MutableList<Shader> = mutableListOf()

    private val vertices = mutableMapOf<String, FloatArray>()
    private val textures = mutableMapOf<String, FloatArray>()
    private val normals = mutableMapOf<String, FloatArray>()

    private var dimension: Int = 2

    fun getDimension() = this.dimension

    fun addShader(shader: Shader): ApplicationBuilder {
        if (!shaders.contains(shader)) {
            shaders.add(shader)
        }
        return this
    }

    fun addVertices(name: String, data: FloatArray): ApplicationBuilder {
        this.vertices[name] = data
        return this
    }

    fun addVertices(name: String, f: () -> FloatArray): ApplicationBuilder {
        addVertices(name, f())
        return this
    }

    private fun addTexture(name: String, data: FloatArray): ApplicationBuilder {
        this.textures[name] = data
        return this
    }

    fun addTexture(name: String, f: () -> FloatArray): ApplicationBuilder {
        addTexture(name, f())
        return this
    }

    private fun addNormals(name: String, data: FloatArray): ApplicationBuilder {
        this.normals[name] = data
        return this
    }

    fun addNormals(name: String, f: () -> FloatArray): ApplicationBuilder {
        addNormals(name, f())
        return this
    }

    fun setDimension(dimension: Int): ApplicationBuilder {
        if (dimension in 2..4) {
            this.dimension = dimension
        } else {
            throw RuntimeException("Dimension of size $dimension not supported.")
        }
        return this
    }

    fun apply(): Application {
        val program = glCreateProgram()
        return Application(program, shaders, vertices, textures, normals, dimension)
    }

}
