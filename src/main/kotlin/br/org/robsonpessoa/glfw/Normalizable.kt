package br.org.robsonpessoa.glfw

interface Normalizable {
    fun configureNormals(builder: ApplicationBuilder)
}
