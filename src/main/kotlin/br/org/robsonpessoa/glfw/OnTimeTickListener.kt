package br.org.robsonpessoa.glfw

interface OnTimeTickListener {
    fun onTimeTick()
}
