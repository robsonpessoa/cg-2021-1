package br.org.robsonpessoa.wavefront

object WavefrontFileReader {

    data class LineDescription(val descriptor: String, val values: List<String>)

    private fun String.isNotComment() = !startsWith("#")

    private fun String.toDescription(): LineDescription? {
        val values = trim().split(regex = Regex("""\s+"""))
        return if (values.isNotEmpty()) {
            LineDescription(values[0], values.subList(1, values.size))
        } else null
    }

    private fun List<String>.toFloatArray(): FloatArray =
        map { str -> str.toFloat() }.toFloatArray()

    fun read(name: String): ObjFile? {
        val faces = mutableListOf<ObjFile.Face>()

        val vertices = mutableListOf<FloatArray>()
        val textures = mutableListOf<FloatArray>()
        val normals = mutableListOf<FloatArray>()

        var material: String? = null

        ClassLoader.getSystemResourceAsStream(name).use {
            it?.bufferedReader()?.forEachLine { line ->
                if (line.isNotComment()) {
                    val description = line.toDescription()
                    when (description?.descriptor) {
                        "v" -> vertices.add(description.values.toFloatArray())
                        "vt" -> textures.add(description.values.toFloatArray())
                        "vn" -> normals.add(description.values.toFloatArray())
                        "usemtl", "usemat" -> material = description.values[0]
                        "f" -> {
                            val face = mutableListOf<Int>()
                            val faceTexture = mutableListOf<Int>()
                            val faceNormals = mutableListOf<Int>()

                            description.values.forEach { str ->
                                val w = str.split("/")
                                face.add(w[0].toInt() - 1)
                                if (w.size >= 2 && w[1].isNotEmpty()) {
                                    faceTexture.add(w[1].toInt() - 1)
                                } else {
                                    faceTexture.add(-1)
                                }
                                faceNormals.add(w[2].toInt() - 1)
                            }

                            faces.add(
                                ObjFile.Face(
                                    vertexIndex = face,
                                    textureIndex = faceTexture,
                                    normalsIndex = faceNormals,
                                    material = material!!
                                )
                            )
                        }
                    }
                }
            }
        }

        return if (vertices.isNotEmpty() || textures.isNotEmpty() || faces.isNotEmpty()) {
            ObjFile(
                vertices = vertices,
                texture = textures,
                normal = normals,
                faces = faces.toTypedArray()
            )
        } else null
    }
}
