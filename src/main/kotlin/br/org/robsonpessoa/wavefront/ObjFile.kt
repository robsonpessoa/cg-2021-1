package br.org.robsonpessoa.wavefront

import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Object3D
import br.org.robsonpessoa.model.Object3DFace
import br.org.robsonpessoa.utils.merge
import org.lwjgl.opengl.GL11.GL_TRIANGLES

data class ObjFile(
    val vertices: List<FloatArray>,
    val texture: List<FloatArray>,
    val normal: List<FloatArray>,
    val faces: Array<Face>
) {

    fun toModel(
        fallbackColor: Engine.Color,
        ambientCoefficient: Float,
        diffuseCoefficient: Float,
        specularCoefficient: Float,
        specularExponent: Float
    ): Object3D {
        return Object3D(
            faces = faces.map {
                val faceVertices = it.vertexIndex.map { v -> vertices[v] }.merge()
                val faceTextures =
                    it.textureIndex.map { v -> if (v != -1) texture[v] else floatArrayOf(-1f, -1f) }.merge()
                val faceNormals =
                    it.normalsIndex.map { v -> if (v != -1) normal[v] else floatArrayOf(1f, 1f, 1f) }.merge()

                Object3DFace(
                    vertices = faceVertices,
                    textures = faceTextures,
                    normals = faceNormals,
                    material = it.material,
                    color = fallbackColor,
                    glDrawMethod = GL_TRIANGLES,
                    ambientCoefficient = ambientCoefficient,
                    diffuseCoefficient = diffuseCoefficient,
                    specularCoefficient = specularCoefficient,
                    specularExponent = specularExponent
                )
            }
        )
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ObjFile

        if (vertices != other.vertices) return false
        if (texture != other.texture) return false
        if (!faces.contentEquals(other.faces)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = vertices.hashCode()
        result = 31 * result + texture.hashCode()
        result = 31 * result + faces.contentHashCode()
        return result
    }

    data class Face(
        val vertexIndex: List<Int>,
        val textureIndex: List<Int>,
        val normalsIndex: List<Int>,
        val material: String
    )
}
