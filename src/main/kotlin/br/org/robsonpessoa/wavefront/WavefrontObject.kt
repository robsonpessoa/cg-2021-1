package br.org.robsonpessoa.wavefront

import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.Object3D


object WavefrontObjectFactory {
    fun create(
        objFilename: String,
        color: Engine.Color = Engine.Color(0f, 0f, 0f, 0f),
        ambientCoefficient: Float = 0.6f,
        diffuseCoefficient: Float = 0.1f,
        specularCoefficient: Float = 0.9f,
        specularExponent: Float = 32f
    ): Object3D {
        return WavefrontFileReader
            .read(objFilename)
            ?.toModel(color, ambientCoefficient, diffuseCoefficient, specularCoefficient, specularExponent)!!
    }
}
