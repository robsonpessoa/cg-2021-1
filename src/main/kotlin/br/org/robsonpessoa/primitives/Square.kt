package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.WithCoordinates
import org.lwjgl.opengl.GL11.GL_TRIANGLE_STRIP
import org.lwjgl.opengl.GL11.glDrawArrays
import java.util.*

class Square(private val edges: FloatArray) : Primitive, WithCoordinates<Pair<Float, Float>> {

    override val id: UUID = UUID.randomUUID()
    override val name: String = "square-$id"
    override var color: Engine.Color? = null

    override fun draw(engine: Engine) {
        engine.getApplication()!![name]?.let {
            color?.let { c -> setColor(engine, c) }
                ?: setColor(engine, engine.foregroundColor)

            glDrawArrays(
                GL_TRIANGLE_STRIP,
                it.start,
                it.length
            )
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            edges
        }
    }

    override fun getCenter(): Pair<Float, Float> {
        val xCords =
            edges.mapIndexed { index, v -> if (index.rem(2) == 0) return@mapIndexed v else null }.filterNotNull()
        val yCords =
            edges.mapIndexed { index, v -> if (index.rem(2) == 1) return@mapIndexed v else null }.filterNotNull()

        val left = xCords.minOrNull()!!
        val right = xCords.maxOrNull()!!

        val top = yCords.maxOrNull()!!
        val bottom = yCords.minOrNull()!!

        return Pair((left + right) / 2, (top + bottom) / 2)
    }

}
