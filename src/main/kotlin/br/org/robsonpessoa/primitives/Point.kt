package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.WithCoordinates
import org.lwjgl.opengl.GL11.GL_POINTS
import org.lwjgl.opengl.GL11.glDrawArrays
import java.util.*

class Point(private val x: Float, private val y: Float) : Primitive, WithCoordinates<Pair<Float, Float>> {

    override val id: UUID = UUID.randomUUID()
    override val name: String = "point-$id"
    override var color: Engine.Color? = null

    companion object {
        private const val DIMENSION = 2
    }

    override fun draw(engine: Engine) {
        engine.getApplication()!![name]?.let {
            color?.let { c -> setColor(engine, c) }
                ?: setColor(engine, engine.foregroundColor)

            glDrawArrays(
                GL_POINTS,
                it.start,
                it.length / DIMENSION
            )
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            floatArrayOf(x, y)
        }
    }

    override fun getCenter(): Pair<Float, Float> {
        return Pair(x, y)
    }

}
