package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.WithCoordinates
import org.lwjgl.opengl.GL11.GL_TRIANGLES
import org.lwjgl.opengl.GL11.glDrawArrays
import java.util.*

class Triangle(
    val first: Pair<Float, Float>,
    val second: Pair<Float, Float>,
    val third: Pair<Float, Float>
) : Primitive, WithCoordinates<Pair<Float, Float>> {

    override val id: UUID = UUID.randomUUID()
    override val name: String = "triangle-$id"
    override var color: Engine.Color? = null

    override fun draw(engine: Engine) {
        engine.getApplication()!![name]?.let {
            color?.let { c -> setColor(engine, c) }
                ?: setColor(engine, engine.foregroundColor)

            glDrawArrays(
                GL_TRIANGLES,
                it.start,
                it.length
            )
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            floatArrayOf(
                first.first, first.second,
                second.first, second.second,
                third.first, third.second
            )
        }
    }

    override fun getCenter(): Pair<Float, Float> {
        return Pair(0f, 0f)
    }

}
