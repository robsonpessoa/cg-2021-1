package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.WithCoordinates
import org.lwjgl.opengl.GL11
import java.util.*
import kotlin.math.cos
import kotlin.math.sin


class Circle(radius: Float, private val x: Float, private val y: Float) : Primitive,
    WithCoordinates<Pair<Float, Float>> {
    private val edges: FloatArray = FloatArray(VERTEX_COUNT * 2)

    override val id: UUID = UUID.randomUUID()
    override val name: String
        get() = "circle-$id"

    override var color: Engine.Color? = null

    companion object {
        private const val VERTEX_COUNT = 32
    }

    init {
        var idx = 0

        var angle = 0.0
        for (i in 0 until VERTEX_COUNT) {
            angle += 2 * Math.PI / VERTEX_COUNT

            val outerX = x + radius * cos(angle)
            val outerY = y + radius * sin(angle)

            edges[idx++] = outerX.toFloat()
            edges[idx++] = outerY.toFloat()
        }
    }

    override fun draw(engine: Engine) {
        engine.getApplication()!![name]?.let {
            color?.let { c -> setColor(engine, c) }
                ?: setColor(engine, engine.foregroundColor)

            GL11.glDrawArrays(
                GL11.GL_TRIANGLE_FAN,
                it.start,
                it.length
            )
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            edges
        }
    }

    override fun getCenter(): Pair<Float, Float> {
        return Pair(x, y)
    }

}
