package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.Drawable
import br.org.robsonpessoa.glfw.Engine
import org.lwjgl.opengl.GL20.glGetUniformLocation
import org.lwjgl.opengl.GL20.glUniform4f
import java.util.*

/**
 * Represents a [Drawable] primitive.
 */
interface Primitive : Drawable {

    companion object {
        const val VERTEX_NAME = "POSITION"
    }

    val id: UUID
    val name: String
    var color: Engine.Color?

    fun setColor(engine: Engine, color: Engine.Color) {
        setColor(engine, color.red, color.green, color.blue, color.alpha)
    }

    fun setColor(engine: Engine, red: Float, green: Float, blue: Float, alpha: Float = 1.0f) {
        val locColor = glGetUniformLocation(engine.getApplication()!!.id, "color")
        glUniform4f(locColor, red, green, blue, alpha)
    }
}
