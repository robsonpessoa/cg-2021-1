package br.org.robsonpessoa.primitives

import br.org.robsonpessoa.glfw.ApplicationBuilder
import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.model.WithCoordinates
import org.lwjgl.opengl.GL11.GL_LINES
import org.lwjgl.opengl.GL11.glDrawArrays
import java.util.*

class Line(
    private val x1: Float, private val y1: Float,
    private val x2: Float, private val y2: Float
) : Primitive, WithCoordinates<Pair<Float, Float>> {

    override val id: UUID = UUID.randomUUID()
    override val name: String = "line-$id"
    override var color: Engine.Color? = null

    companion object {
        private const val DIMENSION = 2
    }

    override fun draw(engine: Engine) {
        engine.getApplication()!![name]?.let {
            color?.let { c -> setColor(engine, c) }
                ?: setColor(engine, engine.foregroundColor)

            glDrawArrays(
                GL_LINES,
                it.start,
                DIMENSION
            )
        }
    }

    override fun configureVertices(builder: ApplicationBuilder) {
        builder.addVertices(name) {
            floatArrayOf(x1, y1, x2, y2)
        }
    }

    override fun getCenter(): Pair<Float, Float> {
        val shiftX = (x1 + x2) / 2
        val shiftY = (y1 + y2) / 2
        return Pair(shiftX, shiftY)
    }

}
