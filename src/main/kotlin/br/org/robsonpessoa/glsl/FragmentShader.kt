package br.org.robsonpessoa.glsl

import br.org.robsonpessoa.glfw.Shader
import org.lwjgl.opengl.GL20

/**
 * Builder to Fragment Shaders.
 *
 */
class FragmentShader : Shader(
    """
        uniform vec3 light_position;
        uniform float ka;
        uniform float kd;
        
        uniform vec3 view_position;
        uniform float ks;
        uniform float ns;
        
        vec3 light_color = vec3(1.0, 1.0, 1.0);
        
        varying vec2 out_texture;
        varying vec3 out_normal;
        varying vec3 out_position;
        
        uniform sampler2D samplerTexture;
        
        void main() {
            // ambient
            vec3 ambient = ka * light_color;
        
            // Diffuse 
            vec3 norm = normalize(out_normal);
            vec3 light_dir = normalize(light_position - out_position);
            float diff = max(dot(norm, light_dir), 0.0);
            vec3 diffuse = kd * diff * light_color;
            
            vec3 view_dir = normalize(view_position - out_position);
            vec3 reflect_dir = normalize(reflect(-light_dir, norm));
            float spec = pow(max(dot(view_dir, reflect_dir), 0.0), ns);
            vec3 specular = ks * spec * light_color;             
           
            vec4 texture = texture2D(samplerTexture, out_texture);
            vec4 result = vec4((ambient + diffuse + specular), 1.0) * texture;
            
            gl_FragColor = result;
        }
    """.trimIndent(), GL20.GL_FRAGMENT_SHADER
)
