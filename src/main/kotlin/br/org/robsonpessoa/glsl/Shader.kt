package br.org.robsonpessoa.glfw

import org.lwjgl.opengl.GL20.*

/**
 * The OpenGL Shader.
 *
 * @property definition the shader type.
 * @property code the shader key.
 */
open class Shader(private val code: String, private val definition: Int) {
    private var mId: Int? = null

    val id: Int
        get() = mId!!

    /**
     * Compiles the shader.
     *
     * @param id the Shader id.
     */
    private fun compile(id: Int) {
        glCompileShader(id)

        val buffer = IntArray(1)
        glGetShaderiv(id, GL_COMPILE_STATUS, buffer)
        if (buffer[0] == 0) {
            val error = glGetShaderInfoLog(id)
            throw RuntimeException("Error while compiling the shader: $error")
        }
    }

    /**
     * Creates the OpenGL shader and attach to the program.
     *
     * @param application the [Application] to attach the shader.
     */
    fun attach(application: Application) {
        val id = glCreateShader(this.definition)
        glShaderSource(id, this.code)
        compile(id)
        mId = id
        glAttachShader(application.id, this.id)
    }
}

fun buildVec4Matrix(name: String, dimension: Int): String =
    when (dimension) {
        2 -> "vec4($name,0.0,1.0)"
        3 -> "vec4($name,1.0)"
        else -> throw RuntimeException("Vector dimension not supported.")
    }

