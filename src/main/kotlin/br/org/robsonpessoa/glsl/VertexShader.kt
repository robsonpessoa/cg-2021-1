package br.org.robsonpessoa.glsl

import br.org.robsonpessoa.glfw.Shader
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.matrix.Projection
import br.org.robsonpessoa.matrix.View
import br.org.robsonpessoa.model.WithTexture
import br.org.robsonpessoa.primitives.Primitive
import org.lwjgl.opengl.GL20

/**
 * Builder to Vertex Shaders.
 *
 */
class VertexShader : Shader(
    """
    uniform mat4 ${Model.VERTEX_NAME};
    uniform mat4 ${View.VERTEX_NAME};
    uniform mat4 ${Projection.VERTEX_NAME};
    
    attribute vec3 ${Primitive.VERTEX_NAME};
    varying vec3 out_position;
    
    attribute vec2 ${WithTexture.VERTEX_NAME};
    varying vec2 out_texture;
    
    attribute vec3 normal;
    varying vec3 out_normal;
    
    void main(){
        gl_Position = ${Projection.VERTEX_NAME}
            * ${View.VERTEX_NAME}
            * ${Model.VERTEX_NAME} 
            * vec4(${Primitive.VERTEX_NAME},1.0);
        
        out_position = vec3(${Model.VERTEX_NAME});
        
        out_texture = vec2(${WithTexture.VERTEX_NAME});
        
        out_normal = vec3(normal);
    }
    """.trimIndent(),
    GL20.GL_VERTEX_SHADER,
)
