/**
 * @author Robson Pessoa <robsonpessoa@usp.br> - 8632564
 */

package br.org.robsonpessoa

import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.work_assignemnts.WorkAssignment2

fun main() {
    val app = Engine()
    val exercise = WorkAssignment2(app)

    app.setWindowConfiguration("Work Assignment 2", 900, 900)
    app.setOnEngineListener(exercise)
    app.getWindow()!!.setOnKeyListener(exercise)
    app.getWindow()!!.setOnMouseListener(exercise)

    app.run()
}

