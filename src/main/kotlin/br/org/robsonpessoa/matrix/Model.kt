package br.org.robsonpessoa.matrix

import br.org.robsonpessoa.math.Matrix
import br.org.robsonpessoa.math.Matrix.Companion.toMatrix
import br.org.robsonpessoa.math.toBuffer
import br.org.robsonpessoa.transformation.Transformation
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20

class Model(private val matrix: Matrix4f = Matrix4f()) {

    companion object {
        const val VERTEX_NAME = "model"
    }

    fun use(id: Int, f: () -> Unit) {
        val loc = GL20.glGetUniformLocation(
            id,
            VERTEX_NAME
        )

        GL20.glUniformMatrix4fv(
            loc, false,
            matrix.toBuffer()
        )

        f()

        GL20.glUniformMatrix4fv(
            loc, true,
            Matrix4f().toBuffer()
        )
    }

    /**
     * Adds another br.org.robsonpessoa.transformation to the current using
     * the product of the two [Matrix]'es
     *
     * @param other the other [Transformation] to be added.
     * @return the resulting Transformation
     */
    @Deprecated("Please use the methods from org.joml.* instead")
    operator fun plus(other: Transformation): Model {
        return Model((this.matrix.toMatrix() * other.matrix).toMatrix4f())
    }
}
