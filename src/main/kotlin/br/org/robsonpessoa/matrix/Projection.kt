package br.org.robsonpessoa.matrix

import br.org.robsonpessoa.math.toBuffer
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20


class Projection(
    height: Int,
    width: Int
) {

    companion object {
        const val VERTEX_NAME = "projection"
    }

    val matrix: Matrix4f

    init {
        val res = Matrix4f()
        res.perspective(
            Math.toRadians(90.0).toFloat(),
            (width / height).toFloat(),
            0.1f,
            40.0f
        )

        matrix = res
    }

    fun apply(id: Int) {
        val loc = GL20.glGetUniformLocation(
            id,
            VERTEX_NAME
        )

        GL20.glUniformMatrix4fv(
            loc, false,
            matrix.toBuffer()
        )
    }
}
