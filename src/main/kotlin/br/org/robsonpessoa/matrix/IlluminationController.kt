package br.org.robsonpessoa.matrix

import org.lwjgl.opengl.GL20

object IlluminationController {
    private var view: View? = null

    private var kaInc = 0f
    private var kdInc = 0f

    private var ambientInc = 0f

    var blockOtherFonts = false

    fun setView(view: View) {
        this.view = view
    }

    fun withIllumination(
        id: Int,
        ambientCoefficient: Float,
        diffuseCoefficient: Float,
        specularCoefficient: Float,
        specularExponent: Float,
        f: () -> Unit
    ) {
        val locKa = GL20.glGetUniformLocation(id, "ka")
        GL20.glUniform1f(locKa, ambientCoefficient + kaInc + ambientInc)

        val locKd = GL20.glGetUniformLocation(id, "kd")
        GL20.glUniform1f(locKd, diffuseCoefficient + kdInc)

        val locKs = GL20.glGetUniformLocation(id, "ks")
        GL20.glUniform1f(locKs, specularCoefficient)

        val locNs = GL20.glGetUniformLocation(id, "ns")
        GL20.glUniform1f(locNs, specularExponent)

        f()
    }

    fun setAmbientLuminanceFactor(value: Float) {
        this.ambientInc = value
    }

    fun increaseKa() {
        kaInc += 0.5f
    }

    fun decreaseKa() {
        kaInc -= 0.5f
    }

    fun increaseKd() {
        kdInc += 0.5f
    }

    fun decreaseKd() {
        kdInc -= 0.5f
    }

    fun refreshIlluminationFont(id: Int, x: Float, y: Float, z: Float) {
        if (!blockOtherFonts) {
            val lightPosition = GL20.glGetUniformLocation(id, "light_position")
            GL20.glUniform3f(lightPosition, x, y, z)
        }
    }

}
