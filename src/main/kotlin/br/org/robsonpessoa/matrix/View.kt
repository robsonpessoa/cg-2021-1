package br.org.robsonpessoa.matrix

import br.org.robsonpessoa.glfw.Window
import br.org.robsonpessoa.math.toBuffer
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.opengl.GL20
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin

class View(
    private var cameraPosition: Vector3f,
    private var cameraFront: Vector3f,
    private var cameraUp: Vector3f
) {

    private var noEventReceived = true

    private var yaw = 0.0
    private var pitch = 0.0

    private var lastX = 0f
    private var lastY = 0f

    private val maxHeight = 19f
    private val bottomLimit = 1f
    private val maxDistance = 20f

    private var leftAvailableFactor = 1f
    private var rightAvailableFactor = 1f
    private var frontAvailableFactor = 1f
    private var backAvailableFactor = 1f

    private var matrix: Matrix4f

    companion object {
        const val VERTEX_NAME = "view"
    }

    init {
        this.matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }

    constructor(
        cameraPosition: FloatArray,
        cameraFront: FloatArray,
        cameraUp: FloatArray
    ) : this(
        cameraPosition = Vector3f(cameraPosition[0], cameraPosition[1], cameraPosition[2]),
        cameraFront = Vector3f(cameraFront[0], cameraFront[1], cameraFront[2]),
        cameraUp = Vector3f(cameraUp[0], cameraUp[1], cameraUp[2])
    )

    fun init(window: Window) {
        lastX = window.width / 2f
        lastY = window.height / 2f
    }

    private fun rebuildMatrix(at: Vector3f, eye: Vector3f, up: Vector3f): Matrix4f {
        checkAvailableDirections(at, eye)

        val res = Matrix4f()

        res.lookAt(at, Vector3f(at).add(eye), up)

        return res
    }

    private fun checkAvailableDirections(position: Vector3f, direction: Vector3f) {
        if (position.y > maxHeight) {
            position.y = maxHeight
            if (direction.y > 0) {
                frontAvailableFactor = 0f
                backAvailableFactor = 1f
            } else {
                frontAvailableFactor = 1f
                backAvailableFactor = 0f
            }
        }

        if (position.y <= bottomLimit) {
            position.y = bottomLimit
            if (direction.y > 0) {
                frontAvailableFactor = 1f
                backAvailableFactor = 0f
            } else {
                frontAvailableFactor = 0f
                backAvailableFactor = 1f
            }
        }

        if (position.y > bottomLimit && position.y < maxHeight) {
            frontAvailableFactor = 1f
            backAvailableFactor = 1f
        }

        if (abs(position.x) >= maxDistance) {
            position.x = if (position.x > 0) maxDistance else -maxDistance
            if (direction.z > 0) {
                leftAvailableFactor = 0f
                rightAvailableFactor = 1f
            } else {
                leftAvailableFactor = 1f
                rightAvailableFactor = 0f
            }
        }

        if (abs(position.z) >= maxDistance) {
            position.z = if (position.z > 0) maxDistance else -maxDistance
            if (direction.x > 0) {
                leftAvailableFactor = 0f
                rightAvailableFactor = 1f
            } else {
                leftAvailableFactor = 1f
                rightAvailableFactor = 0f
            }
        }

        if (abs(position.x) <= maxDistance && abs(position.z) <= maxDistance) {
            leftAvailableFactor = 1f
            rightAvailableFactor = 1f
        }
    }

    fun refresh(id: Int) {
        val loc = GL20.glGetUniformLocation(
            id,
            VERTEX_NAME
        )

        GL20.glUniformMatrix4fv(
            loc, false,
            matrix.toBuffer()
        )

        refreshIlluminationFont(id)
    }

    private enum class LightFonts {
        EXTERNAL,
        HOUSE
    }

    private var currentLightFont: LightFonts = LightFonts.EXTERNAL

    private fun refreshIlluminationFont(id: Int) {
        currentLightFont = if (cameraPosition.x > 7.8f && cameraPosition.x < 18f &&
            cameraPosition.y < 5.14f &&
            cameraPosition.z < 8.76 && cameraPosition.z > 2f
        ) {
            LightFonts.HOUSE
        } else {
            LightFonts.EXTERNAL
        }

        if (currentLightFont == LightFonts.HOUSE) {
            IlluminationController.refreshIlluminationFont(id, 12.9f, 5.10f, 5.38f)
            IlluminationController.blockOtherFonts = true
        } else {
            IlluminationController.blockOtherFonts = false
        }
    }

    fun zoomIn(speed: Float) {
        cameraPosition = cameraPosition.add(Vector3f(cameraFront).mul(speed).mul(frontAvailableFactor))
        matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }

    fun zoomOut(speed: Float) {
        cameraPosition = cameraPosition.sub(Vector3f(cameraFront).mul(speed).mul(backAvailableFactor))
        matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }

    fun left(speed: Float) {
        cameraPosition =
            cameraPosition.sub(Vector3f(cameraFront).cross(cameraUp).normalize().mul(speed).mul(leftAvailableFactor))
        matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }

    fun right(speed: Float) {
        cameraPosition =
            cameraPosition.add(Vector3f(cameraFront).cross(cameraUp).normalize().mul(speed).mul(rightAvailableFactor))
        matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }

    fun look(x: Float, y: Float) {
        if (noEventReceived) {
            noEventReceived = false
            lastX = x
            lastY = y
        }

        var xoffset = x - lastX
        var yoffset = lastY - y
        lastX = x
        lastY = y

        val sensitivity = 0.1f
        xoffset *= sensitivity
        yoffset *= sensitivity

        yaw += xoffset;
        pitch += yoffset;

        if (pitch >= 90.0)
            pitch = 90.0
        if (pitch <= -90.00)
            pitch = -90.0

        setCameraFront(
            (cos(Math.toRadians(yaw)) * cos(Math.toRadians(pitch))).toFloat(),
            (sin(Math.toRadians(pitch))).toFloat(),
            (sin(Math.toRadians(yaw)) * cos(Math.toRadians(pitch))).toFloat()
        )
    }

    private fun setCameraFront(x: Float, y: Float, z: Float) {
        val res = Vector3f()
        res.x = x
        res.y = y
        res.z = z
        cameraFront = res.normalize()
        this.matrix = rebuildMatrix(cameraPosition, cameraFront, cameraUp)
    }
}
