package br.org.robsonpessoa.transformation

import br.org.robsonpessoa.math.IdentityMatrix
import br.org.robsonpessoa.math.Matrix
import org.lwjgl.opengl.GL20
import java.nio.FloatBuffer

/**
 * Class that represents transformation operations through Transformation [Matrix]'es.
 *
 * @property matrix - the initial Matrix (default is [IdentityMatrix]) to be used in the Transformation
 */
@Deprecated("Please use the methods from org.joml.* instead")
open class Transformation(var matrix: Matrix = IdentityMatrix(DIMENSION)) {

    companion object {
        const val DIMENSION = 4
    }

    /**
     * Returns the Transformation Matrix that represents the current object br.org.robsonpessoa.transformation.
     *
     * @return the br.org.robsonpessoa.transformation [Matrix].
     */
    fun getTransformationMatrix(): Matrix = this.matrix

    /**
     * Gets the current Transformation Matrix as a [FloatBuffer]
     *
     * @return the Matrix as a buffer
     */
    open fun toFloatBuffer(): FloatBuffer = matrix.toBuffer()

    /**
     * Adds another br.org.robsonpessoa.transformation to the current using the product of the two [Matrix]'es
     *
     * @param other the other [Transformation] to be added.
     * @return the resulting Transformation
     */
    open operator fun plus(other: Transformation): Transformation {
        return Transformation(this.matrix * other.matrix)
    }

    /**
     * Uses the matrix transformation by sending the current data to OpenGL,
     * then executes the function passed and finally replace the data passed to OpenGL
     * to the IdentityMatrix (cleaning the OpenGL buffer).
     *
     * @param name the Name of the variable in the Vertex Shader
     * @param id the Program id
     * @param f the function to be executed
     */
    protected fun use(name: String, id: Int, f: () -> Unit) {
        val loc = GL20.glGetUniformLocation(
            id,
            name
        )

        GL20.glUniformMatrix4fv(
            loc, true,
            toFloatBuffer()
        )

        f()

        GL20.glUniformMatrix4fv(
            loc, true,
            IdentityMatrix(DIMENSION).toBuffer()
        )
    }
}
