package br.org.robsonpessoa.transformation

import br.org.robsonpessoa.glfw.Engine
import br.org.robsonpessoa.matrix.Model
import br.org.robsonpessoa.primitives.Primitive

/**
 * Enfolds a [Primitive] with the possibility to apply a [Transformation]
 * when drawing itself.
 */
class MutablePrimitive<out T : Primitive>(
    private val primitive: T,
    private var model: Model = Model()
) :
    Primitive by primitive {

    override fun draw(engine: Engine) {
        val programId = engine.getApplication()!!.id

        model.use(programId) {
            primitive.draw(engine)
        }
    }

    @Deprecated("Please use the methods from org.joml.* instead")
    fun apply(transformation: Transformation) {
        this.model = this.model + transformation
    }

    fun getPrimitive(): T {
        return this.primitive
    }
}
