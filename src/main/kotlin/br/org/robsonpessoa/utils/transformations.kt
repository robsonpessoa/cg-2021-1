package br.org.robsonpessoa.utils

import org.lwjgl.BufferUtils
import java.nio.FloatBuffer

fun FloatArray.toBuffer(): FloatBuffer {
    val buffer = BufferUtils.createFloatBuffer(size)
    buffer.put(this).flip()
    return buffer
}

fun Collection<FloatArray>.merge(): FloatArray {
    val size = fold(0) { acc, e ->
        acc + e.size
    }
    val result = FloatArray(size)
    var offset = 0
    return fold(result) { acc, e ->
        e.copyInto(result, offset)
        offset += e.size
        return@fold result
    }
}
